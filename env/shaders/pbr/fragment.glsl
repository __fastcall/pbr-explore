/*
 * Reference used for Specular BRDF equations:
 * http://graphicrants.blogspot.com/2013/08/specular-brdf-reference.html
 *
 */

#version 410 core

#define PI 3.14159265358979323846
#define MAX_LOD_AO_DIFFUSE 8.0
#define MAX_LOD_AO_SPECULAR 6.0

#define NO_ZERO(x) (max(x, 0.00001))

/* Received values. */
in vec3 worldPosition;
in vec2 uv;
in vec3 worldNormal;

/* Uniform matrices */
uniform mat4 model;

/* Uniform values */
uniform float ambientOcclusionValue;
uniform bool useAmbientOcclusionValue;
uniform vec3 albedoColor;
uniform bool useAlbedoColor;
uniform float metalnessValue;
uniform bool useMetalnessValue;
uniform float glossinessValue;
uniform bool useGlossinessValue;

/* Uniform texture samplers. */
uniform sampler2D ambientOcclusionTexture;
uniform sampler2D albedoTexture;
uniform sampler2D metalnessTexture;
uniform sampler2D glossinessTexture;

/* Scene uniform values. */
uniform sampler2D envMapTexture;
uniform float exposure;
uniform float gamma;
uniform bool noEnvMap;
uniform vec3 lightPosition;
uniform vec3 cameraPosition;
uniform float lightIntensity;
uniform vec3 lightColor;
uniform bool isDirectional;

/* Debugging uniforms. */
uniform int equation;

/* BRDF response look-up table (split sum approximation). */
uniform sampler2D brdfResponse;

/* Fragment output color. */
layout(location = 0) out vec4 color;

/* Samples equirectangular texture and returns UV coordinates on 2D texture. */
vec2 SampleEquirectangular(vec3 v) {
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= vec2(0.5 / PI, 1.0 / PI);
    uv += 0.5;
    return uv;
}

/* Normal distribution function (Trowbridge-Reitz GGX). */
float NDF_TRGGX(float a2, float n_h) {
	float part = n_h * n_h * (a2 - 1.0) + 1.0;
	return a2 / (PI * part * part);
}

/* Geometry function (Schlick GGX). */
float G_SchlickGGX(float n_v, float k) {
	return n_v / (n_v * (1.0 - k) + k);
}

/* Geometry function (Smith). */
float G_Smith(float n_v, float n_l, float k) {
	return G_SchlickGGX(n_v, k) * G_SchlickGGX(n_l, k);
}

/* Fresnel function (Schlick). */
vec3 F_Schlick(vec3 F0, float h_v) {
	float part = 1.0 - h_v;
	float part2 = part * part;
	return F0 + (1.0 - F0) * part2 * part2 * part;
}

/* Fresnel function with roughness (Schlick). */
vec3 F_SchlickRoughness(vec3 F0, float n_v, float roughness) {
	float part = 1.0 - n_v;
	float part2 = part * part;
	return F0 + (max(vec3(1.0 - roughness), F0) - F0) * part2 * part2 * part;
}

void main() {
	
	/* Light and camera vectors. */
	vec3 n = normalize(worldNormal); /* World normal direction. */
	vec3 l = isDirectional ? normalize(lightPosition) : normalize(lightPosition - worldPosition); /* Direction to light. */
	vec3 v = normalize(cameraPosition - worldPosition); /* Direction to camera (view). */
	vec3 h = normalize(l + v); /* Halfway vector. */
	
	/* Common dot products. */
	float n_h = NO_ZERO(dot(n, h));
	float n_v = NO_ZERO(dot(n, v));
	float n_l = NO_ZERO(dot(n, l));
	float h_v = NO_ZERO(dot(h, v));
	
	/* Ambient occlusion factor. */
	float ao = ambientOcclusionValue;
	if (!useAmbientOcclusionValue) {
		ao = texture(ambientOcclusionTexture, uv).x;
	}
	
	/* Albedo color. */
	vec3 albedo = albedoColor;
	if (!useAlbedoColor) {
		albedo = texture(albedoTexture, uv).xyz;
	}
	
	/* Roughness, alpha and k factor. */
	float roughness = 1.0 - glossinessValue;
	if (!useGlossinessValue) {
		roughness = 1.0 - texture(glossinessTexture, uv).x;
	}
	float a = roughness * roughness;
	float a2 = a * a;
	float k = a2 / 2.0;
	
	/* Metalness factor. */
	float metalness = metalnessValue;
	if (!useMetalnessValue) {
		metalness = texture(metalnessTexture, uv).x;
	}
	
	/* Normal distribution and geometry. */
	float NDF = NDF_TRGGX(a2, n_h);
	float G = G_Smith(n_v, n_l, k);
	
	/* Base reflectivity. */
	vec3 F0 = mix(vec3(0.04), albedo, metalness);
	vec3 F = F_Schlick(F0, h_v);
	
	/* Specular and diffuse values. */
	vec3 specular = NDF * G * F / (4.0 * n_v * n_l);
	vec3 diffuse = (vec3(1.0) - F) * (1.0 - metalness);
	
	/* Calculate light radiance. */
	float distance = length(lightPosition - worldPosition);
	float attenuation = lightIntensity * lightIntensity;
	if (!isDirectional) {
		attenuation /= distance * distance;
	}
	vec3 radiance = lightColor * attenuation;
	
	/* Calculate light color. */
	vec3 lColor = (diffuse * albedo / PI + specular) * radiance * n_l;
	
	/* Set ambient color. */
	vec3 ambient = vec3(0.0);

	/* Calculate ambient color. */
	vec3 diffuseAmb, specularAmb, Famb = F_SchlickRoughness(F0, n_v, roughness);
	if (!noEnvMap) {

		/* Calculate ambient diffuse. */
		diffuseAmb = (1.0 - Famb) * (1.0 - metalness) * textureLod(envMapTexture, SampleEquirectangular(normalize(worldNormal - (model * vec4(0.0)).xyz)), MAX_LOD_AO_DIFFUSE).rgb * albedo;
		
		/* Calculate ambient specular. */
		vec3 filteredColor = textureLod(envMapTexture, SampleEquirectangular(reflect(-v, n)), roughness * MAX_LOD_AO_SPECULAR).rgb;
		vec2 brdf = texture(brdfResponse, vec2(n_v, roughness)).xy;
		specularAmb = filteredColor * (Famb * brdf.x + brdf.y);

		/* Write ambient color. */
		ambient = (diffuseAmb + specularAmb) * ao;

	}
	
	/* Calculate linear color. */
	vec3 hdrColor = lColor + ambient;
	
	/* Tone mapping based on exposure. */
	vec3 toneMap = vec3(1.0) - exp(-hdrColor * exposure);
	
	/* Gamma correction (to sRGB space). */
	vec3 gammaCorr = pow(toneMap, vec3(1.0 / gamma));
	
	/* Pass output color. */
	switch (equation) {
	case 0:
		color = vec4(gammaCorr, 1.0);
		break;
	case 1:
		color = vec4(vec3(NDF), 1.0);
		break;
	case 2:
		color = vec4(vec3(G), 1.0);
		break;
	case 3:
		color = vec4(F, 1.0);
		break;
	case 4:
		color = vec4(Famb, 1.0);
		break;
	case 5:
		color = vec4(diffuseAmb, 1.0);
		break;
	case 6:
		color = vec4(specularAmb, 1.0);
		break;
	}
	
}
