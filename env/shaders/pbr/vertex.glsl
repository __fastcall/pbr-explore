#version 410 core

/* Input data stream. */
layout (location = 0) in vec3 localPosition;
layout (location = 1) in vec2 localUV;
layout (location = 2) in vec3 localNormal;

/* Uniform variables. */
uniform mat4 perspective;
uniform mat4 view;
uniform mat4 model;

/* Propagated values. */
out vec3 worldPosition;
out vec2 uv;
out vec3 worldNormal;

void main() {

	/* Calculate world position and pass to fragment shader. */
	worldPosition = (model * vec4(localPosition, 1.0)).xyz;
	
	/* Pass UV coordinates to fragment shader. */
	uv = localUV;
	
	/* Calculate world normals and pass to fragment shader. */
	worldNormal = (model * vec4(localNormal, 1.0)).xyz;
	
	/* Set position for interpolation. */
	gl_Position = perspective * view * vec4(worldPosition, 1.0);
	
}
