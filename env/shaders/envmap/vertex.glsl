#version 410 core

/* Input data stream. */
layout (location = 0) in vec3 localPosition;

/* Uniform variables. */
uniform mat4 view; /* Contains rotation matrix from camera's global rotation quaternion. */
uniform mat4 model; /* Contains scaling matrix for scaling the plane based on window width and height. */

/* Propagated values. */
out vec3 worldDirection;

void main() {
	
	/* Calculate world direction and pass to fragment shader. */
	worldDirection = (view * model * vec4(localPosition, 1.0)).xyz;
	
	/* Set position for interpolation. */
	gl_Position = vec4(localPosition.xy, 1.0, 1.0); /* Component z = w, to optimize depth testing, allowing environemnt map to be drawn only if nothing was drawn at that fragment. */
	
}
