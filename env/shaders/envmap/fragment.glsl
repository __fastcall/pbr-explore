#version 410 core

#define PI 3.14159265358979323846

/* Received values. */
in vec3 worldDirection;

/* Scene uniform values. */
uniform sampler2D envMapTexture;
uniform float exposure;
uniform float gamma;

/* Fragment output color. */
layout(location = 0) out vec4 color;

/* Samples equirectangular texture and returns UV coordinates on 2D texture. */
vec2 SampleEquirectangular(vec3 v) {
    vec2 uv = vec2(atan(v.z, v.x), asin(v.y));
    uv *= vec2(0.5 / PI, 1.0 / PI);
    uv += 0.5;
    return uv;
}

void main() {
	
	/* Get UV coordinates from equirectangular coordinates. */
	vec2 uv = SampleEquirectangular(normalize(worldDirection));
	
	/* Sample environment map. */
	vec3 hdrColor = textureLod(envMapTexture, uv, 0.0).xyz;
	
	/* Tone mapping based on exposure. */
	vec3 toneMap = vec3(1.0) - exp(-hdrColor * exposure);
	
	/* Gamma correction (to sRGB space). */
	vec3 gammaCorr = pow(toneMap, vec3(1.0 / gamma));
	
	/* Pass output color. */
	color = vec4(gammaCorr, 1.0);
	
}
