The following environment maps were acquired from HDRIHaven:
https://hdrihaven.com

They are licensed under CC0 license:
https://creativecommons.org/publicdomain/zero/1.0/
https://creativecommons.org/publicdomain/zero/1.0/legalcode
