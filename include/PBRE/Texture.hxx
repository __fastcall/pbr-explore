#ifndef __PBRE_TEXTURE_HXX__
#define __PBRE_TEXTURE_HXX__

#include "TextureWrapMode.hxx"
#include "TextureFilter.hxx"
#include <GL/glew.h>
#include <string>

namespace PBRE {

    /**
     * @brief Represents a texture.
     */
    class Texture {

    /**
     * @brief %Program class can access private members. 
     */
    friend class Program;

    public:

        /**
         * @brief Constructs a new texture from an image file.
         * @param imagepath Image path.
         * @param s Horizontal wrap mode.
         * @param t Vertical wrap mode.
         * @param min Minification filter.
         * @param mag Magnification filter.
         */
        Texture(const std::string& imagepath, TextureWrapMode s = TextureWrapMode::Repeat, TextureWrapMode t = TextureWrapMode::Repeat, TextureFilter min = TextureFilter::Trilinear, TextureFilter mag = TextureFilter::Bilinear);

        /**
         * @brief Constructs a new texture by moving from another texture instance.
         * @param texture Original texture.
         */
        Texture(Texture&& texture);

        /**
         * @brief Move assignemnt operator.
         * @param texture Original texture.
         * @return Texture& Reference to this texture.
         */
        Texture& operator=(Texture&& texture);

        /**
         * @brief Destroys native texture handles.
         */
        ~Texture();

        /**
         * @brief Sets the texture wrap mode.
         * @param s Horizontal wrap mode.
         * @param t Vertical wrap mode.
         */
        void SetTextureWrapMode(TextureWrapMode s, TextureWrapMode t);

        /**
         * @brief Sets the texture filtering.
         * @param min Minification filter.
         * @param max Magnification filter.
         */
        void SetTextureFilter(TextureFilter min, TextureFilter max);

    private:

        Texture(const Texture&) = delete;

        Texture& operator=(const Texture&) = delete;

        GLuint m_Texture;

        static GLenum GetInternalWrapMode(TextureWrapMode mode);

        static GLenum GetInternalFilter(TextureFilter filter);

    };

}

#endif
