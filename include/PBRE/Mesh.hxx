#ifndef __PBRE_MESH_HXX__
#define __PBRE_MESH_HXX__

#include "Texture.hxx"
#include <cstddef>
#include <glm/glm.hpp>
#include <GL/glew.h>
#include <string>
#include <memory>

namespace PBRE {

    /**
     * @brief Represents a mesh object.
     */
    class Mesh {

    /**
     * @brief %Program can access private members.
     */
    friend class Program;

    public:

        /**
         * @brief Constructs a new mesh object from model file.
         * @note If model file contains multiple meshes, it loads the first one.
         * @param filename Model file.
         */
        Mesh(const std::string& filename);

        /**
         * @brief Constructs a new mesh from raw vertices/indices.
         * @param verts Vertex array.
         * @param uvs Texture coordinate array.
         * @param normals Normal array.
         * @param nVerts Number of vertices/texture coordinates/normals.
         * @param indices Indices array.
         * @param nIndices Number of indices.
         */
        Mesh(const glm::vec3* verts, const glm::vec2* uvs, const glm::vec3* normals, std::size_t nVerts, const glm::uvec3* indices, std::size_t nIndices);

        /**
         * @brief Construct a new mesh object by moving from a different mesh instance.
         * @param mesh Original mesh.
         */
        Mesh(Mesh&& mesh);

        /**
         * @brief Move assignment operator.
         * @param mesh Original mesh.
         * @return Mesh& Reference to this mesh.
         */
        Mesh& operator=(Mesh&& mesh);

        /**
         * @brief Destroys all native handles.
         */
        ~Mesh();

        /**
         * @brief Static ambient occlusion factor [0, 1].
         */
        float AmbientOcclusionValue;

        /**
         * @brief Use static ambient occlusion factor.
         */
        bool UseAmbientOcclusionValue;

        /**
         * @brief Static albedo color (RGB float).
         */
        glm::vec3 AlbedoColor;

        /**
         * @brief Use static albedo color.
         */
        bool UseAlbedoColor;

        /**
         * @brief Static metalness factor [0, 1].
         */
        float MetalnessValue;

        /**
         * @brief Use static metalness factor.
         */
        bool UseMetalnessValue;

        /**
         * @brief Static glossiness factor [0, 1].
         * @note glossiness = 1 - rougness
         */
        float GlossinessValue;

        /**
         * @brief Use static glossiness factor.
         */
        bool UseGlossinessValue;

    private:

        Mesh(const Mesh&) = delete;

        Mesh& operator=(const Mesh&) = delete;

        std::unique_ptr<Texture> m_AmbientOcclusionTexture;

        std::unique_ptr<Texture> m_AlbedoTexture;

        std::unique_ptr<Texture> m_MetalnessTexture;

        std::unique_ptr<Texture> m_GlossinessTexture;

        GLuint m_VAO, m_VBO, m_EBO;

        unsigned int m_Count;

    };

}

#endif
