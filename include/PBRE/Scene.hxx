#ifndef __PBRE_SCENE_HXX__
#define __PBRE_SCENE_HXX__

#include "Spatial.hxx"
#include <memory>

namespace PBRE {

    /**
     * @brief Represents a complete 3D scene.
     */
    class Scene {

    /**
     * @brief %Program can access private methods.
     */
    friend class Program;

    public:

        /**
         * @brief Constructs a scene.
         */
        Scene();

        /**
         * @brief Get the root object.
         * @return Spatial* Root object.
         */
        inline Spatial* GetRoot() { return m_Root.get(); }

        /**
         * @brief Perspective matrix.
         */
        glm::mat4 Perspective;

        /**
         * @brief View matrix.
         */
        glm::mat4 View;

    private:

        std::unique_ptr<Spatial> m_Root;

    };

}

#endif
