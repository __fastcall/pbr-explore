#ifndef __PBRE_SPATIAL_HXX__
#define __PBRE_SPATIAL_HXX__

#include <glm/glm.hpp>
#include <glm/gtc/quaternion.hpp>
#include "Mesh.hxx"
#include <memory>
#include <list>

namespace PBRE {

    /**
     * @brief Represents a spatial object (object in 3D space).
     */
    class Spatial {

    /**
     * @brief %Scene class can access private memebers.
     */
    friend class Scene;

    /**
     * @brief %Program class can access private memebers.
     */
    friend class Program;

    public:

        /**
         * @brief Constructs a new spatial object from mesh.
         * @param mesh Shared mesh reference.
         */
        Spatial(const std::shared_ptr<Mesh>& mesh);

        /**
         * @brief Sets the mesh of the spatial object.
         * @param mesh New mesh.
         */
        void SetMesh(const std::shared_ptr<Mesh>& mesh);

        /**
         * @brief @brief Adds a new child.
         * @param pos Initial position.
         * @return Spatial* Added child.
         */
        Spatial* Add(glm::vec3 pos = glm::vec3());

        /**
         * @brief Removes a child.
         * @param child Targeted child.
         * @return true Child was removed.
         * @return false Element is not a child and it was not removed.
         */
        bool Remove(Spatial* child);

        /**
         * @brief Translates the object.
         * @param delta Delta translation.
         */
        void Translate(glm::vec3 delta);

        /**
         * @brief Rotates the object around axis.
         * @param axis Targeted axis.
         * @param a Amount in radians.
         */
        void Rotate(glm::vec3 axis, float a);

        /**
         * @brief Gets the local rotation in euler angles.
         * @return glm::vec3 Local rotation in euler angles.
         */
        glm::vec3 RotationEuler();

        /**
         * @brief Sets the local rotation in euler angles.
         * @param euler Local rotation in euler angles.
         */
        void RotationEuler(glm::vec3 euler);

        /**
         * @brief Scales the object.
         * @param scale Multiplied scale.
         */
        void Scale(glm::vec3 scale);

        /**
         * @brief Get the global forward axis.
         * @return glm::vec3 Forward axis.
         */
        glm::vec3 GetForwardAxis();

        /**
         * @brief Get the global right axis.
         * @return glm::vec3 right axis.
         */
        glm::vec3 GetRightAxis();

        /**
         * @brief Get the global up axis.
         * @return glm::vec3 up axis.
         */
        glm::vec3 GetUpAxis();

        /**
         * @brief Get the global rotation matrix.
         * @return glm::mat Global rotation matrix.
         */
        inline glm::mat4 GetRotationMatrix() { return glm::mat4_cast(DecomposeRotation()); }

        /**
         * @brief Object position in 3D space.
         */
        glm::vec3 Position;

        /**
         * @brief Object rotation in 3D space (quaternion).
         */
        glm::quat Rotation;

        /**
         * @brief Object scale in 3D space.
         */
        glm::vec3 Scaling;

    private:

        Spatial(glm::vec3 pos = glm::vec3());

        void UpdateMatrices();

        glm::quat DecomposeRotation();

        Spatial* m_Parent;

        std::list<Spatial> m_Children;

        std::shared_ptr<Mesh> m_Mesh;

        glm::mat4 m_Model;

        glm::mat4 m_CachedModel;

    };

}

#endif
