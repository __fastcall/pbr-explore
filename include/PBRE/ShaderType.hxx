#ifndef __PBRE_SHADERTYPE_HXX__
#define __PBRE_SHADERTYPE_HXX__

namespace PBRE {

    /**
     * @brief Enumberates accepted shader types.
     */
    enum class ShaderType {

        /**
         * @brief Represents a vertex GLSL shader.
         */
        Vertex,

        /**
         * @brief Represents a fragment GLSL shader.
         */
        Fragment

    };

}

#endif
