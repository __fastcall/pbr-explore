#ifndef __PBRE_TEXTUREFILTER_HXX__
#define __PBRE_TEXTUREFILTER_HXX__

namespace PBRE {

    /**
     * @brief Enumerates accepted texture filtering modes.
     */
    enum class TextureFilter {

        /**
         * @brief No filtering and no mipmaps.
         */
        Nearest,

        /**
         * @brief Linear filtering and no mipmaps (bilinear).
         */
        Linear,

        /**
         * @brief Alias of linear.
         */
        Bilinear,

        /**
         * @brief No filtering, sharp switching between mipmaps.
         */
        NearestMipmapNearest,

        /**
         * @brief No filtering, smooth transition between mipmaps.
         */
        NearestMipmapLinear,

        /**
         * @brief Linear filtering, sharp switching between mipmaps (bilinear with mipmaps).
         */
        LinearMipmapNearest,

        /**
         * @brief Alias of linear mipmap nearest.
         */
        BilinearWithMipmaps,

        /**
         * @brief Linear filtering, smooth transition between mipmaps (trilinear).
         */
        LinearMipmapLinear,

        /**
         * @brief Alias of linear mipmap linear.
         */
        Trilinear

    };

}

#endif
