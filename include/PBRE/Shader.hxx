#ifndef __PBRE_SHADER_HXX__
#define __PBRE_SHADER_HXX__

#include "ShaderType.hxx"
#include "ShaderHandle.hxx"
#include <string>
#include <memory>

namespace PBRE {

    /**
     * @brief Usable handle to shader object.
     */
    class Shader {

    /**
     * @brief %Program can access private members.
     */
    friend class Program;

    public:

        /**
         * @brief Constructs a new shader from source file path and type.
         * @param sourcefile Source file type.
         * @param type Type of the shader.
         */
        Shader(const std::string& sourcefile, ShaderType type);

        /**
         * @brief Constructs a new shader by copying another shader.
         * @param shader Original shader.
         */
        Shader(const Shader& shader);

        /**
         * @brief Constructs a new shader by moving handles from another shader.
         * @param shader Original shader.
         */
        Shader(Shader&& shader);

        /**
         * @brief Copy assignment operator.
         * @param shader Original shader.
         * @return Shader& Reference to this shader.
         */
        Shader& operator=(const Shader& shader);

        /**
         * @brief Move assignment operator.
         * @param shader Original shader.
         * @return Shader& Reference to this shader.
         */
        Shader& operator=(Shader&& shader);

        /**
         * @brief Gets the type of the shader.
         * @return ShaderType Type of the shader.
         */
        inline ShaderType GetType() { return m_ShaderHandle->m_Type; }

    private:

        std::shared_ptr<ShaderHandle> m_ShaderHandle;

    };

}

#endif
