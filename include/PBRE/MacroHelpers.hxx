#ifndef __PBRE_MACROHELPERS_HXX__
#define __PBRE_MACROHELPERS_HXX__

#include <iostream>
#include <stdexcept>

/**
 * @brief Terminates process if expression <strong>c</strong> doesn't evaluate to <strong>s</strong>, with <strong>m</strong> message.
 */
#define MUST_SUCCEED(c, s, m) {if ((c) != (s)) { std::cerr << m << std::endl; std::terminate(); }}

/**
 * @brief Terminates process if expression <strong>c</strong> evaluates to <strong>f</strong>, with <strong>m</strong> message.
 */
#define MUST_NOT_FAIL(c, f, m) {if ((c) == (f)) { std::cerr << m << std::endl; std::terminate(); }}

/**
 * @brief Throws exception if expression <strong>c</strong> doesn't evaluate to <strong>s</strong>, with <strong>m</strong> message.
 */
#define SHOULD_SUCCEED(c, s, m) {if ((c) != (s)) throw std::runtime_error(m); }

/**
 * @brief Throws exception if expression <strong>c</strong> evaluates to <strong>f</strong>, with <strong>m</strong> message.
 */
#define SHOULD_NOT_FAIL(c, f, m) {if ((c) == (f)) throw std::runtime_error(m); }

#endif
