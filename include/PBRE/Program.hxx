#ifndef __PBRE_PROGRAM_HXX__
#define __PBRE_PROGRAM_HXX__

#include "Scene.hxx"
#include "Shader.hxx"
#include <string>

namespace PBRE {

    /**
     * @brief Represents a shading program.
     */
    class Program {

    public:

        /**
         * @brief Constructs a new shading program by searching program directory for source files.
         * @param dir Shader program directory name.
         */
        Program(const std::string& dir);

        /**
         * @brief Constructs a new shading program from vertex and fragment shader.
         * @param vertex Vertex shader.
         * @param fragment Fragment shader.
         */
        Program(const Shader& vertex, const Shader& fragment);

        /**
         * @brief Constructs a new shader program by moving handles from another shader program.
         * @param program Original shader program.
         */
        Program(Program&& program);

        /**
         * @brief Move assignment operator.
         * @param program Original shader program.
         * @return Program& Reference to this shader program.
         */
        Program& operator=(Program&& program);

        /**
         * @brief Destroys all native shader program handles.
         */
        ~Program();
        
        /**
         * @brief Renders a 3D scene.
         * @param scene Targeted scene.
         * @param envMap Enivornment map.
         * @param exposure Exposure value.
         * @param gamma Gamma value.
         * @param noEnvMap Don't reflect environment map.
         */
        void RenderScene(Scene& scene, std::shared_ptr<Texture> envMap = std::shared_ptr<Texture>(nullptr), float exposure = 1.0f, float gamma = 2.2f, bool noEnvMap = false);

        /**
         * @brief Renders a single transformed mesh.
         * @param mesh Targeted mesh.
         * @param trans Transformation matrix (view).
         * @param trans2 Transformation matrix 2 (model).
         * @param envMap Environment map.
         * @param exposure Exposure value.
         * @param gamma Gamma value.
         */
        void RenderMesh(const Mesh& mesh, const glm::mat4& trans = glm::mat4(1.0f), const glm::mat4& trans2 = glm::mat4(1.0f), std::shared_ptr<Texture> envMap = std::shared_ptr<Texture>(nullptr), float exposure = 1.0f, float gamma = 2.2f);

        /**
         * @brief Sets the camera position (for PBR shader).
         * @param position Camera position.
         */
        void SetCameraPosition(const glm::vec3& position);

        /**
         * @brief Sets the light position (for PBR shader).
         * @param position Light position.
         */
        void SetLightPosition(const glm::vec3& position);

        /**
         * @brief Set light intensity (for PBR shader).
         * @param intensity Light intensity.
         */
        void SetLightIntensity(float intensity);

        /**
         * @brief Sets the light color.
         * @param color Light color.
         */
        void SetLightColor(const glm::vec3& color);

        /**
         * @brief Sets the light directional.
         * @param isDirectional Is directional.
         */
        void SetLightDirectional(bool isDirectional);

        /**
         * @brief Set the BRDF response texture look-up table (of split sum approximation).
         * @param texture BRDF response texture.
         */
        void SetBrdfResponseTexture(const Texture& texture);

        /**
         * @brief Sets the displayed equation.
         * @param equation Displayed equation.
         */
        void SetEquation(int equation);

        /**
         * @brief Search path for shader program source files.
         */
        static std::string ProgramSearchPath;

    private:

        Program(const Program&) = delete;

        Program& operator=(const Program&) = delete;

        void RenderSpatialRecursive(Spatial& spatial);

        GLuint m_Program;

        GLuint m_Perspective;

        GLuint m_View;

        GLuint m_Model;

        GLuint m_AmbientOcclusionValue;

        GLuint m_UseAmbientOcclusionValue;

        GLuint m_AlbedoColor;

        GLuint m_UseAlbedoColor;

        GLuint m_MetalnessValue;

        GLuint m_UseMetalnessValue;

        GLuint m_GlossinessValue;

        GLuint m_UseGlossinessValue;

        GLuint m_AmbientOcclusionTexture;

        GLuint m_AlbedoTexture;

        GLuint m_MetalnessTexture;

        GLuint m_GlossinessTexture;

        GLuint m_EnvMapTexture;

        GLuint m_Exposure;

        GLuint m_Gamma;

        GLuint m_NoEnvMap;

        GLuint m_LightPosition;

        GLuint m_CameraPosition;

        GLuint m_LightIntensity;

        GLuint m_LightColor;

        GLuint m_IsDirectional;

        GLuint m_BRDFResponse;

        GLuint m_Equation;

    };

}

#endif
