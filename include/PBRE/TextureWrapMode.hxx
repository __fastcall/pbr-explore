#ifndef __PBRE_TEXTUREWRAPMODE_HXX__
#define __PBRE_TEXTUREWRAPMODE_HXX__

namespace PBRE {

    /**
     * @brief Enumeration of supported texture wrapping modes.
     */
    enum class TextureWrapMode {

        /**
         * @brief Repeats texture without mirroring.
         */
        Repeat,

        /**
         * @brief Repeats texture with mirroring.
         */
        MirroredRepeat,

        /**
         * @brief Clamps texture to edge.
         */
        ClampToEdge

    };

}

#endif
