#ifndef __PBRE_SHADERHANDLE_HXX__
#define __PBRE_SHADERHANDLE_HXX__

#include "ShaderType.hxx"
#include <GL/glew.h>
#include <string>

namespace PBRE {

    /**
     * @brief Native handle to shader object.
     */
    class ShaderHandle {

    /**
     * @brief %Shader can access private members.
     */
    friend class Shader;

    /**
     * @brief %Program can access private members.
     */
    friend class Program;

    public:

        /**
         * @brief Destroys a shader handle object.
         */
        ~ShaderHandle();

    private:

        ShaderHandle(const std::string& sourcefile, ShaderType type);

        ShaderHandle(const ShaderHandle&) = delete;

        ShaderHandle(ShaderHandle&& shaderHandle);

        ShaderHandle& operator=(const ShaderHandle&) = delete;

        ShaderHandle& operator=(ShaderHandle&& shaderHandle);

        ShaderType m_Type;

        GLuint m_Shader;

        static GLenum GetInternalShaderType(ShaderType type);

    };

}

#endif
