#define GLM_ENABLE_EXPERIMENTAL
#include <glm/gtx/matrix_decompose.hpp>
#include <PBRE/Spatial.hxx>

namespace PBRE {

    Spatial::Spatial(glm::vec3 pos) {

        /* Initialize spatial. */
        m_Parent = nullptr;

        /* Initialize position. */
        Position = pos;

        /* Initialize rotation. */
        Rotation = glm::identity<glm::quat>();

        /* Initialize scale. */
        Scaling.x = Scaling.y = Scaling.z = 1.0f;

        /* Update matrices. */
        UpdateMatrices();

    }

    Spatial::Spatial(const std::shared_ptr<Mesh>& mesh) : Spatial() {

        /* Initialize mesh */
        m_Mesh = mesh;

    }

    void Spatial::SetMesh(const std::shared_ptr<Mesh>& mesh) {

        /* Set mesh */
        m_Mesh = mesh;

    }

    Spatial* Spatial::Add(glm::vec3 pos) {

        /* Add a new child and return it. */
        m_Children.push_back(Spatial(pos));
        Spatial* child = &(m_Children.back());
        child->m_Parent = this;
        return child;

    }

    bool Spatial::Remove(Spatial* child) {

        /* Remove a child. */
        for (std::list<Spatial>::iterator it = m_Children.begin(); it != m_Children.end(); ++it) {
            if (&(*it) == child) {
                m_Children.erase(it);
                return true;
            }
        }
        return false;

    }

    void Spatial::Translate(glm::vec3 delta) {

        /* Translates by delta. */
        Position += delta;

    }

    void Spatial::Rotate(glm::vec3 axis, float a) {

        /* Rotates around axis. */
        Rotation = glm::rotate(Rotation, a, axis);

    }

    glm::vec3 Spatial::RotationEuler() {

        /* Return euler angles (pitch as x, yaw as y, roll as z) */
        return glm::eulerAngles(Rotation);

    }

    void Spatial::RotationEuler(glm::vec3 euler) {

        /* Set local rotation to euler angles. */
        Rotation = glm::quat(euler);

    }

    void Spatial::Scale(glm::vec3 scale) {

        /* Scale object. */
        Scaling *= scale;

    }

    glm::vec3 Spatial::GetForwardAxis() {

        /* Get forward axis. */
        return DecomposeRotation() * glm::vec3(0.0f, 0.0f, -1.0f);

    }

    glm::vec3 Spatial::GetRightAxis() {

        /* Get right axis. */
        return DecomposeRotation() * glm::vec3(1.0f, 0.0f, 0.0f);

    }

    glm::vec3 Spatial::GetUpAxis() {

        /* Get up axis. */
        return DecomposeRotation() * glm::vec3(0.0f, 1.0f, 0.0f);

    }

    void Spatial::UpdateMatrices() {

        /* Update matrix. */
        m_Model = glm::mat4(1.0);
        m_Model = glm::scale(m_Model, Scaling);
        m_Model = glm::mat4_cast(Rotation) * m_Model;
        m_Model = glm::translate(m_Model, Position);

        /* Update cached hierarchy matrix. */
        if (m_Parent == nullptr) {
            m_CachedModel = m_Model;
        } else {
            m_CachedModel = m_Parent->m_CachedModel * m_Model;
        }

    }

    glm::quat Spatial::DecomposeRotation() {

        /* Decompose model matrix. */
        glm::vec3 scale;
        glm::quat rot;
        glm::vec3 trans;
        glm::vec3 skew;
        glm::vec4 persp;
        glm::decompose(m_CachedModel, scale, rot, trans, skew, persp);
        return glm::conjugate(rot); /* Conjugate is returnet instead of actual rotation quaternion (GLM bug). */

    }

}
