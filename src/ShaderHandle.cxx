#include <PBRE/ShaderHandle.hxx>
#include <PBRE/MacroHelpers.hxx>
#include <fstream>

namespace PBRE {

    ShaderHandle::ShaderHandle(const std::string& sourcefile, ShaderType type) {

        /* Create OpenGL shader. */
        m_Type = type;
        SHOULD_NOT_FAIL(m_Shader = glCreateShader(GetInternalShaderType(type)), 0, "Failed to create shader.")

        /* Source file buffer and length. */
        char* source = nullptr;
        int len;

        /* Try to read file. */
        try {

            /* Open file stream and calculate file length. */
            std::ifstream ifs(sourcefile, std::ios_base::in | std::ios_base::binary);
            ifs.seekg(0, std::ios_base::end);
            len = ifs.tellg();
            ifs.seekg(0, std::ios_base::beg);

            /* Allocate buffer and read data. */
            source = new char[len];
            ifs.read(source, len);

        } catch (const std::exception& ex) {

            /* Free buffer. */
            delete[] source;
            source = nullptr;

            /* Delete shader. */
            glDeleteShader(m_Shader);
            m_Shader = 0;

            /* Pass exception. */
            throw ex;

        }

        /* Load and compile shader source. */
        glShaderSource(m_Shader, 1, &source, &len);
        glCompileShader(m_Shader);

        /* Free buffer. */
        delete[] source;
        source = nullptr;

        /* Check for errors. */
        GLint success;
        glGetShaderiv(m_Shader, GL_COMPILE_STATUS, &success);
        if (success == GL_FALSE) {

            /* Get info log length. */
            GLint length;
            glGetShaderiv(m_Shader, GL_INFO_LOG_LENGTH, &length);

            /* Get info log. */
            char log[length];
            glGetShaderInfoLog(m_Shader, length, &length, log);

            /* Destroy shader. */
            glDeleteShader(m_Shader);
            m_Shader = 0;

            /* Throw exception with log. */
            throw std::logic_error(log);

        }

    }

    ShaderHandle::ShaderHandle(ShaderHandle&& shaderHandle) {

        /* Move native OpenGL shader handle. */
        m_Type = shaderHandle.m_Type;
        m_Shader = shaderHandle.m_Shader;
        shaderHandle.m_Shader = 0;

    }

    ShaderHandle& ShaderHandle::operator=(ShaderHandle&& shaderHandle) {

        /* Move native OpenGL shader handle. */
        m_Type = shaderHandle.m_Type;
        glDeleteShader(m_Shader);
        m_Shader = shaderHandle.m_Shader;
        shaderHandle.m_Shader = 0;
        return *this;

    }

    ShaderHandle::~ShaderHandle() {

        /* Delete native OpenGL shader handle. */
        glDeleteShader(m_Shader);
        m_Shader = 0;

    }

    GLenum ShaderHandle::GetInternalShaderType(ShaderType type) {

        /* Decode shader type constant. */
        switch (type) {
        case ShaderType::Vertex:
            return GL_VERTEX_SHADER;
        case ShaderType::Fragment:
            return GL_FRAGMENT_SHADER;
        }
        return 0;

    }

}
