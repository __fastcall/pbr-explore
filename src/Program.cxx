#include <PBRE/Program.hxx>
#include <PBRE/MacroHelpers.hxx>
#include <glm/gtc/type_ptr.hpp>

namespace PBRE {

    Program::Program(const std::string& dir) : Program(Shader(ProgramSearchPath + "/" + dir + "/vertex.glsl", ShaderType::Vertex), Shader(ProgramSearchPath + "/" + dir + "/fragment.glsl", ShaderType::Fragment)) {
        
        /* No code. */

    }

    Program::Program(const Shader& vertex, const Shader& fragment) {

        /* Create OpenGL program. */
        SHOULD_NOT_FAIL(m_Program = glCreateProgram(), 0, "Failed to create program.")

        /* Attach shaders. */
        glAttachShader(m_Program, vertex.m_ShaderHandle->m_Shader);
        glAttachShader(m_Program, fragment.m_ShaderHandle->m_Shader);

        /* Link program. */
        glLinkProgram(m_Program);

        /* Check for errors. */
        GLint success;
        glGetProgramiv(m_Program, GL_LINK_STATUS, &success);
        if (success == GL_FALSE) {

            /* Get info log length. */
            GLint length;
            glGetProgramiv(m_Program, GL_INFO_LOG_LENGTH, &length);

            /* Get info log. */
            char log[length];
            glGetProgramInfoLog(m_Program, length, &length, log);

            /* Destroy program. */
            glDeleteProgram(m_Program);
            m_Program = 0;

            /* Throw exception with log. */
            throw std::logic_error(log);

        }

        /* Get shader uniform locations. */
        m_Perspective = glGetUniformLocation(m_Program, "perspective");
        m_View = glGetUniformLocation(m_Program, "view");
        m_Model = glGetUniformLocation(m_Program, "model");
        m_AmbientOcclusionValue = glGetUniformLocation(m_Program, "ambientOcclusionValue");
        m_UseAmbientOcclusionValue = glGetUniformLocation(m_Program, "useAmbientOcclusionValue");
        m_AlbedoColor = glGetUniformLocation(m_Program, "albedoColor");
        m_UseAlbedoColor = glGetUniformLocation(m_Program, "useAlbedoColor");
        m_MetalnessValue = glGetUniformLocation(m_Program, "metalnessValue");
        m_UseMetalnessValue = glGetUniformLocation(m_Program, "useMetalnessValue");
        m_GlossinessValue = glGetUniformLocation(m_Program, "glossinessValue");
        m_UseGlossinessValue = glGetUniformLocation(m_Program, "useGlossinessValue");
        m_AmbientOcclusionTexture = glGetUniformLocation(m_Program, "ambientOcclusionTexture");
        m_AlbedoTexture = glGetUniformLocation(m_Program, "albedoTexture");
        m_MetalnessTexture = glGetUniformLocation(m_Program, "metalnessTexture");
        m_GlossinessTexture = glGetUniformLocation(m_Program, "glossinessTexture");
        m_EnvMapTexture = glGetUniformLocation(m_Program, "envMapTexture");
        m_Exposure = glGetUniformLocation(m_Program, "exposure");
        m_Gamma = glGetUniformLocation(m_Program, "gamma");
        m_NoEnvMap = glGetUniformLocation(m_Program, "noEnvMap");
        m_LightPosition = glGetUniformLocation(m_Program, "lightPosition");
        m_CameraPosition = glGetUniformLocation(m_Program, "cameraPosition");
        m_LightIntensity = glGetUniformLocation(m_Program, "lightIntensity");
        m_LightColor = glGetUniformLocation(m_Program, "lightColor");
        m_IsDirectional = glGetUniformLocation(m_Program, "isDirectional");
        m_BRDFResponse = glGetUniformLocation(m_Program, "brdfResponse");
        m_Equation = glGetUniformLocation(m_Program, "equation");

    }

    Program::Program(Program&& program) {

        /* Move program. */
        m_Program = program.m_Program;
        program.m_Program = 0;

        /* Move uniform locations. */
        m_Perspective = program.m_Perspective;
        m_View = program.m_View;
        m_Model = program.m_Model;
        m_AmbientOcclusionValue = program.m_AmbientOcclusionValue;
        m_UseAmbientOcclusionValue = program.m_UseAmbientOcclusionValue;
        m_AlbedoColor = program.m_AlbedoColor;
        m_UseAlbedoColor = program.m_UseAlbedoColor;
        m_MetalnessValue = program.m_MetalnessValue;
        m_UseMetalnessValue = program.m_UseMetalnessValue;
        m_GlossinessValue = program.m_GlossinessValue;
        m_UseGlossinessValue = program.m_UseGlossinessValue;
        m_AmbientOcclusionTexture = program.m_AmbientOcclusionTexture;
        m_AlbedoTexture = program.m_AlbedoTexture;
        m_MetalnessTexture = program.m_MetalnessTexture;
        m_GlossinessTexture = program.m_GlossinessTexture;
        m_EnvMapTexture = program.m_EnvMapTexture;
        m_Exposure = program.m_Exposure;
        m_Gamma = program.m_Gamma;
        m_NoEnvMap = program.m_NoEnvMap;
        m_LightPosition = program.m_LightPosition;
        m_CameraPosition = program.m_CameraPosition;
        m_LightIntensity = program.m_LightIntensity;
        m_LightColor = program.m_LightColor;
        m_IsDirectional = program.m_IsDirectional;
        m_BRDFResponse = program.m_BRDFResponse;
        m_Equation = program.m_Equation;

    }

    Program& Program::operator=(Program&& program) {

        /* Move program. */
        glDeleteProgram(m_Program);
        m_Program = program.m_Program;
        program.m_Program = 0;

        /* Move uniform locations. */
        m_Perspective = program.m_Perspective;
        m_View = program.m_View;
        m_Model = program.m_Model;
        m_AmbientOcclusionValue = program.m_AmbientOcclusionValue;
        m_UseAmbientOcclusionValue = program.m_UseAmbientOcclusionValue;
        m_AlbedoColor = program.m_AlbedoColor;
        m_UseAlbedoColor = program.m_UseAlbedoColor;
        m_MetalnessValue = program.m_MetalnessValue;
        m_UseMetalnessValue = program.m_UseMetalnessValue;
        m_GlossinessValue = program.m_GlossinessValue;
        m_UseGlossinessValue = program.m_UseGlossinessValue;
        m_AmbientOcclusionTexture = program.m_AmbientOcclusionTexture;
        m_AlbedoTexture = program.m_AlbedoTexture;
        m_MetalnessTexture = program.m_MetalnessTexture;
        m_GlossinessTexture = program.m_GlossinessTexture;
        m_EnvMapTexture = program.m_EnvMapTexture;
        m_Exposure = program.m_Exposure;
        m_Gamma = program.m_Gamma;
        m_NoEnvMap = program.m_NoEnvMap;
        m_LightPosition = program.m_LightPosition;
        m_CameraPosition = program.m_CameraPosition;
        m_LightIntensity = program.m_LightIntensity;
        m_LightColor = program.m_LightColor;
        m_IsDirectional = program.m_IsDirectional;
        m_BRDFResponse = program.m_BRDFResponse;
        m_Equation = program.m_Equation;

        /* Return reference to this instance. */
        return *this;

    }

    Program::~Program() {

        /* Destroy program. */
        glDeleteProgram(m_Program);
        m_Program = 0;

    }

    void Program::RenderScene(Scene& scene, std::shared_ptr<Texture> envMap, float exposure, float gamma, bool noEnvMap) {

        /* Clear framebuffer. */
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

        /* Use this program. */
        glUseProgram(m_Program);

        /* Set camera projection and view matrix. */
        glUniformMatrix4fv(m_Perspective, 1, false, reinterpret_cast<float*>(&scene.Perspective));
        glUniformMatrix4fv(m_View, 1, false, reinterpret_cast<float*>(&scene.View));

        /* Activate environment map texture. */
        if (envMap.get() != nullptr) {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, envMap->m_Texture);
        }

        /* Set exposure and gamma. */
        glUniform1f(m_Exposure, exposure);
        glUniform1f(m_Gamma, gamma);

        /* Enable or disable envmap reflections. */
        glUniform1i(m_NoEnvMap, noEnvMap);

        /* Activate all textures. */
        glUniform1i(m_EnvMapTexture, 0);
        glUniform1i(m_AmbientOcclusionTexture, 1);
        glUniform1i(m_AlbedoTexture, 2);
        glUniform1i(m_MetalnessTexture, 3);
        glUniform1i(m_GlossinessTexture, 4);
        glUniform1i(m_BRDFResponse, 5);

        /* Render scene recursive. */
        RenderSpatialRecursive(*(scene.m_Root.get()));

        /* Unset used program. */
        glUseProgram(0);

    }

    void Program::RenderMesh(const Mesh& mesh, const glm::mat4& trans, const glm::mat4& trans2, std::shared_ptr<Texture> envMap, float exposure, float gamma) {

        /* Use this program. */
        glUseProgram(m_Program);

        /* Set camera projection and view matrix. */
        glUniformMatrix4fv(m_View, 1, false, reinterpret_cast<const float*>(&trans));
        glUniformMatrix4fv(m_Model, 1, false, reinterpret_cast<const float*>(&trans2));

        /* Activate environment map texture. */
        if (envMap.get() != nullptr) {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, envMap->m_Texture);
        }

        /* Set exposure and gamma. */
        glUniform1f(m_Exposure, exposure);
        glUniform1f(m_Gamma, gamma);

        /* Activate all textures. */
        glUniform1i(m_EnvMapTexture, 0);
        glUniform1i(m_AmbientOcclusionTexture, 1);
        glUniform1i(m_AlbedoTexture, 2);
        glUniform1i(m_MetalnessTexture, 3);
        glUniform1i(m_GlossinessTexture, 4);
        glUniform1i(m_BRDFResponse, 5);

        /* Set uniform values. */
        glUniform1f(m_AmbientOcclusionValue, mesh.AmbientOcclusionValue);
        glUniform1i(m_UseAmbientOcclusionValue, mesh.UseAmbientOcclusionValue);
        glUniform3fv(m_AlbedoColor, 1, glm::value_ptr(mesh.AlbedoColor));
        glUniform1i(m_UseAlbedoColor, mesh.UseAlbedoColor);
        glUniform1f(m_MetalnessValue, mesh.MetalnessValue);
        glUniform1i(m_UseMetalnessValue, mesh.UseMetalnessValue);
        glUniform1f(m_GlossinessValue, mesh.GlossinessValue);
        glUniform1i(m_UseGlossinessValue, mesh.UseGlossinessValue);

        /* Set uniform texture samplers. */
        if (mesh.m_AmbientOcclusionTexture.get() != nullptr) {
            glActiveTexture(GL_TEXTURE1);
            glBindTexture(GL_TEXTURE_2D, mesh.m_AmbientOcclusionTexture->m_Texture);
        }
        if (mesh.m_AlbedoTexture.get() != nullptr) {
            glActiveTexture(GL_TEXTURE2);
            glBindTexture(GL_TEXTURE_2D, mesh.m_AlbedoTexture->m_Texture);
        }
        if (mesh.m_MetalnessTexture.get() != nullptr) {
            glActiveTexture(GL_TEXTURE3);
            glBindTexture(GL_TEXTURE_2D, mesh.m_MetalnessTexture->m_Texture);
        }
        if (mesh.m_GlossinessTexture.get() != nullptr) {
            glActiveTexture(GL_TEXTURE4);
            glBindTexture(GL_TEXTURE_2D, mesh.m_GlossinessTexture->m_Texture);
        }

        /* Draw elements. */
        glBindVertexArray(mesh.m_VAO);
        glDrawElements(GL_TRIANGLES, mesh.m_Count, GL_UNSIGNED_INT, nullptr);
        glBindVertexArray(0);

        /* Unset used program. */
        glUseProgram(0);

    }

    void Program::SetCameraPosition(const glm::vec3& position) {

        /* Use this program. */
        glUseProgram(m_Program);

        /* Set camera position uniform. */
        glUniform3fv(m_CameraPosition, 1, glm::value_ptr(position));

        /* Unset used program. */
        glUseProgram(0);

    }

    void Program::SetLightPosition(const glm::vec3& position) {

        /* Use this program. */
        glUseProgram(m_Program);

        /* Set light position uniform. */
        glUniform3fv(m_LightPosition, 1, glm::value_ptr(position));

        /* Unset used program. */
        glUseProgram(0);

    }

    void Program::SetLightIntensity(float intensity) {

        /* Use this program. */
        glUseProgram(m_Program);

        /* Set light intensity uniform. */
        glUniform1f(m_LightIntensity, intensity);

        /* Unset used program. */
        glUseProgram(0);

    }

    void Program::SetLightColor(const glm::vec3& color) {

        /* Use this program. */
        glUseProgram(m_Program);

        /* Set light color uniform. */
        glUniform3fv(m_LightColor, 1, glm::value_ptr(color));

        /* Unset used program. */
        glUseProgram(0);

    }

    void Program::SetLightDirectional(bool isDirectional) {

        /* Use this program. */
        glUseProgram(m_Program);

        /* Set light directional uniform. */
        glUniform1i(m_IsDirectional, isDirectional);

        /* Unset used program. */
        glUseProgram(0);

    }

    void Program::SetBrdfResponseTexture(const Texture& texture) {

        /* Use this program. */
        glUseProgram(m_Program);

        /* Set BRDF response texture. */
        glActiveTexture(GL_TEXTURE5);
        glBindTexture(GL_TEXTURE_2D, texture.m_Texture);

        /* Unset used program. */
        glUseProgram(0);

    }

    void Program::SetEquation(int equation) {

        /* Use this program. */
        glUseProgram(m_Program);

        /* Set BRDF response texture. */
        glUniform1i(m_Equation, equation);

        /* Unset used program. */
        glUseProgram(0);

    }

    void Program::RenderSpatialRecursive(Spatial& spatial) {

        /* Update spatial matrices. */
        spatial.UpdateMatrices();

        /* Draw spatial object if possible. */
        if (spatial.m_Mesh.get() != nullptr) {

            /* Set model matrix uniform. */
            glUniformMatrix4fv(m_Model, 1, false, reinterpret_cast<float*>(&spatial.m_CachedModel));

            /* Set uniform values. */
            glUniform1f(m_AmbientOcclusionValue, spatial.m_Mesh->AmbientOcclusionValue);
            glUniform1i(m_UseAmbientOcclusionValue, spatial.m_Mesh->UseAmbientOcclusionValue);
            glUniform3fv(m_AlbedoColor, 1, glm::value_ptr(spatial.m_Mesh->AlbedoColor));
            glUniform1i(m_UseAlbedoColor, spatial.m_Mesh->UseAlbedoColor);
            glUniform1f(m_MetalnessValue, spatial.m_Mesh->MetalnessValue);
            glUniform1i(m_UseMetalnessValue, spatial.m_Mesh->UseMetalnessValue);
            glUniform1f(m_GlossinessValue, spatial.m_Mesh->GlossinessValue);
            glUniform1i(m_UseGlossinessValue, spatial.m_Mesh->UseGlossinessValue);

            /* Set uniform texture samplers. */
            if (spatial.m_Mesh->m_AmbientOcclusionTexture.get() != nullptr) {
                glActiveTexture(GL_TEXTURE1);
                glBindTexture(GL_TEXTURE_2D, spatial.m_Mesh->m_AmbientOcclusionTexture->m_Texture);
            }
            if (spatial.m_Mesh->m_AlbedoTexture.get() != nullptr) {
                glActiveTexture(GL_TEXTURE2);
                glBindTexture(GL_TEXTURE_2D, spatial.m_Mesh->m_AlbedoTexture->m_Texture);
            }
            if (spatial.m_Mesh->m_MetalnessTexture.get() != nullptr) {
                glActiveTexture(GL_TEXTURE3);
                glBindTexture(GL_TEXTURE_2D, spatial.m_Mesh->m_MetalnessTexture->m_Texture);
            }
            if (spatial.m_Mesh->m_GlossinessTexture.get() != nullptr) {
                glActiveTexture(GL_TEXTURE4);
                glBindTexture(GL_TEXTURE_2D, spatial.m_Mesh->m_GlossinessTexture->m_Texture);
            }

            /* Draw elements. */
            glBindVertexArray(spatial.m_Mesh->m_VAO);
            glDrawElements(GL_TRIANGLES, spatial.m_Mesh->m_Count, GL_UNSIGNED_INT, nullptr);
            glBindVertexArray(0);

        }

        /* Render children recursive. */
        for (Spatial& child : spatial.m_Children) {
            RenderSpatialRecursive(child);
        }

    }

    std::string Program::ProgramSearchPath = "shaders";

}
