#include <PBRE/MacroHelpers.hxx>
#include <PBRE/Scene.hxx>
#include <PBRE/Program.hxx>
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <imgui.h>
#include <imgui_impl_opengl3.h>
#include <imgui_impl_glfw.h>
#include <memory>
#include <string>
#include <algorithm>
#include <glm/gtc/type_ptr.hpp>

#define FOVY (glm::pi<float>() / 4.0f)

namespace PBRE {

    extern unsigned char g_IconArray[];

}

static PBRE::Program* g_PBR = nullptr;
static glm::vec3 g_LightPosition = glm::vec3(15.0f, 5.0f, 10.0f);
static float g_LightIntensity = 10.0f;
static glm::vec3 g_LightColor = glm::vec3(1.0f, 1.0f, 1.0f);
static bool g_Directional = false;

static GLFWwindow* g_Window;
static PBRE::Scene* g_Scene = nullptr;
static PBRE::Spatial* g_Camera = nullptr;
static PBRE::Spatial* g_Child = nullptr;
static float g_DeltaTime = 0.0f;

const std::string g_ModelPath = "models";
std::shared_ptr<PBRE::Mesh> g_Sphere;
std::shared_ptr<PBRE::Mesh> g_Teapot;
std::shared_ptr<PBRE::Mesh> g_Monkey;
std::shared_ptr<PBRE::Mesh> g_Custom;

const std::string g_EnvMapPath = "envmaps";
std::shared_ptr<PBRE::Texture> g_Spruit_Sunrise;
std::shared_ptr<PBRE::Texture> g_Cape_Hill;
std::shared_ptr<PBRE::Texture> g_Artist_Workshop;
std::shared_ptr<PBRE::Texture> g_CustomEM;

std::shared_ptr<PBRE::Texture> g_BRDFResponse;

inline static void UpdateProjection(int w, int h) {

    /* Update projection matrix. */
    g_Scene->Perspective = glm::perspectiveFovRH(FOVY, (float)w, (float)h, 0.01f, 100.0f);

}

static void ResizeCallback(GLFWwindow* win, int w, int h) {

    /* Adapt OpenGL viewport. */
    glViewport(0, 0, w, h);

    /* Update projection matrix. */
    UpdateProjection(w, h);

}

inline static void UpdateView() {

    /* Update view matrix. */
    g_Scene->View = glm::lookAtRH(g_Camera->Position, g_Camera->Position + g_Camera->GetForwardAxis(), glm::vec3(0.0f, 1.0f, 0.0f));
    g_PBR->SetCameraPosition(g_Camera->Position);

}

static void MouseWheelCallback(GLFWwindow* win, double xoffset, double yoffset) {

    /* Scroll speed. */
    const float scrollSpeed = 0.5f;

    /* Mouse wheel callback. */
    g_Camera->Position += scrollSpeed * (float)yoffset * g_Camera->GetForwardAxis();

}

static void HandleCameraMovement() {

    /* Camera speeds. */
    float speed = (glfwGetKey(g_Window, GLFW_KEY_LEFT_SHIFT)) ? 5.0f : 1.5f;
    const float rotateSpeed = 0.0075f;
    const float panSpeed = 0.01f;

    /* Handle forward and backward camera movement. */
    if (glfwGetKey(g_Window, GLFW_KEY_W) == GLFW_PRESS) {
        g_Camera->Position += speed * g_DeltaTime * g_Camera->GetForwardAxis();
    } else if (glfwGetKey(g_Window, GLFW_KEY_S) == GLFW_PRESS) {
        g_Camera->Position -= speed * g_DeltaTime * g_Camera->GetForwardAxis();
    }

    /* Handle right and left camera movement. */
    if (glfwGetKey(g_Window, GLFW_KEY_D) == GLFW_PRESS) {
        g_Camera->Position += speed * g_DeltaTime * g_Camera->GetRightAxis();
    } else if (glfwGetKey(g_Window, GLFW_KEY_A) == GLFW_PRESS) {
        g_Camera->Position -= speed * g_DeltaTime * g_Camera->GetRightAxis();
    }

    /* Handle rotating and panning the camera. */
    static bool firstMouseClickR = true;
    static bool firstMouseClickM = true;
    if (glfwGetMouseButton(g_Window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
        static double px, py, cx, cy;
        glfwGetCursorPos(g_Window, &cx, &cy);
        if (firstMouseClickR) {
            firstMouseClickR = false;
        } else {
            float cosAngle = glm::dot(g_Camera->GetForwardAxis(), glm::vec3(0.0f, 1.0f, 0.0f));
            if (((cosAngle < 0.925f) && (cy - py < 0)) || ((cosAngle > -0.925f) && (cy - py > 0)))
                g_Camera->Rotate(g_Camera->GetRightAxis(), rotateSpeed * (float)(cy - py));
            g_Camera->Rotate(glm::vec3(0.0f, 1.0f, 0.0f), rotateSpeed * (float)(cx - px));
        }
        px = cx, py = cy;
    } else if (glfwGetMouseButton(g_Window, GLFW_MOUSE_BUTTON_MIDDLE) == GLFW_PRESS) {
        static double px, py, cx, cy;
        glfwGetCursorPos(g_Window, &cx, &cy);
        if (firstMouseClickM) {
            firstMouseClickM = false;
        } else {
            g_Camera->Translate(panSpeed * (-g_Camera->GetRightAxis() * (float)(cx - px) + g_Camera->GetUpAxis() * (float)(cy - py)));
        }
        px = cx, py = cy;
    } else {
        firstMouseClickR = true;
        firstMouseClickM = true;
    }

    /* Update view matrix. */
    UpdateView();

}

inline static bool endsWith(std::string str, std::string ext) {
    std::transform(str.begin(), str.end(), str.begin(), ::tolower);
    std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
    return (str.rfind(ext) == str.length() - ext.length());
}

static void dropFile(GLFWwindow* win, int n, const char** files) {

    /* Must be exactly one file. */
    if (n != 1) {
        return;
    }

    /* Get file name. */
    std::string myFile = files[0];

    /* Check if accepted model. */
    if (
        (endsWith(myFile, ".fbx")) ||
        (endsWith(myFile, ".dae")) ||
        (endsWith(myFile, ".gltf")) ||
        (endsWith(myFile, ".glb")) ||
        (endsWith(myFile, ".blend")) ||
        (endsWith(myFile, ".3ds")) ||
        (endsWith(myFile, ".ase")) ||
        (endsWith(myFile, ".obj")) ||
        (endsWith(myFile, ".ifc")) ||
        (endsWith(myFile, ".xgl")) ||
        (endsWith(myFile, ".zgl")) ||
        (endsWith(myFile, ".ply")) ||
        (endsWith(myFile, ".dxf")) ||
        (endsWith(myFile, ".lwo")) ||
        (endsWith(myFile, ".lws")) ||
        (endsWith(myFile, ".lxo")) ||
        (endsWith(myFile, ".stl")) ||
        (endsWith(myFile, ".x")) ||
        (endsWith(myFile, ".ac")) ||
        (endsWith(myFile, ".ms3d")) ||
        (endsWith(myFile, ".cob")) ||
        (endsWith(myFile, ".scm"))
    ) {
        
        /* Load custom PBR model. */
        g_Custom = std::shared_ptr<PBRE::Mesh>(new PBRE::Mesh(myFile));

    } else if (
        (endsWith(myFile, ".jpeg")) ||
        (endsWith(myFile, ".jpg")) ||
        (endsWith(myFile, ".png")) ||
        (endsWith(myFile, ".tga")) ||
        (endsWith(myFile, ".bmp")) ||
        (endsWith(myFile, ".psd")) ||
        (endsWith(myFile, ".gif")) ||
        (endsWith(myFile, ".hdr")) ||
        (endsWith(myFile, ".pic")) ||
        (endsWith(myFile, ".ppm")) ||
        (endsWith(myFile, ".pgm"))
    ) {

        /* Load custom env map. */
        g_CustomEM = std::shared_ptr<PBRE::Texture>(new PBRE::Texture(myFile));

    }

}

static std::shared_ptr<PBRE::Mesh> g_SelectedMeshRef;
static int g_SelectedMesh = 0;
static bool g_AutoRotate = false;
static float g_RotationSpeed = 1.0f;

static std::shared_ptr<PBRE::Texture> g_SelectedEnvMapRef;
static int g_SelectedEnvMap = 0;
static float g_Exposure = 1.0f;
static float g_Gamma = 2.2f;

static bool g_DisableMSAA = false;
static bool g_WireframeOnly = false;
static bool g_NoEnvMap = false;
static bool g_DisableCulling = false;

static void DrawGUI() {

    /* Get GLFW window size. */
    int w, h;
    glfwGetWindowSize(g_Window, &w, &h);

    /* Begin model window. */
    ImGui::SetNextWindowPos(ImVec2(0.0f, 0.0f));
    ImGui::SetNextWindowSize(ImVec2(285.0f, (float)h));
    ImGui::Begin("Model", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoNav);

    /* Select model mesh. */
    ImGui::Text("MESH");
    if (ImGui::RadioButton("Sphere", g_SelectedMesh == 0)) {
        g_SelectedMeshRef = g_Sphere;
        g_Child->SetMesh(g_Sphere);
        g_SelectedMesh = 0;
    }
    if (ImGui::RadioButton("Teapot", g_SelectedMesh == 1)) {
        g_SelectedMeshRef = g_Teapot;
        g_Child->SetMesh(g_Teapot);
        g_SelectedMesh = 1;
    }
    if (ImGui::RadioButton("Monkey", g_SelectedMesh == 2)) {
        g_SelectedMeshRef = g_Monkey;
        g_Child->SetMesh(g_Monkey);
        g_SelectedMesh = 2;
    }
    if ((g_Custom.get() != nullptr) && (ImGui::RadioButton("Custom", g_SelectedMesh == 3))) {
        g_SelectedMeshRef = g_Custom;
        g_Child->SetMesh(g_Custom);
        g_SelectedMesh = 3;
    }
    ImGui::Separator();

    /* Allow animating object. */
    ImGui::Text("ANIMATION");
    ImGui::Checkbox("Auto-Rotate", &g_AutoRotate);
    if (g_AutoRotate)
        ImGui::SliderFloat("rad/s", &g_RotationSpeed, 0.1f, 5.0f);
    ImGui::Separator();

    /* Set up PBR parameters. */
    ImGui::Text("PBR PARAMETERS");
    ImGui::Checkbox("Use Constant AO", &(g_SelectedMeshRef->UseAmbientOcclusionValue));
    if (g_SelectedMeshRef->UseAmbientOcclusionValue)
        ImGui::SliderFloat("AO", &(g_SelectedMeshRef->AmbientOcclusionValue), 0.0f, 1.0f);
    ImGui::Checkbox("Use Constant Albedo", &(g_SelectedMeshRef->UseAlbedoColor));
    if (g_SelectedMeshRef->UseAlbedoColor)
        ImGui::ColorPicker3("Albedo", glm::value_ptr(g_SelectedMeshRef->AlbedoColor));
    ImGui::Checkbox("Use Constant Metalness", &(g_SelectedMeshRef->UseMetalnessValue));
    if (g_SelectedMeshRef->UseMetalnessValue)
        ImGui::SliderFloat("Metalness", &(g_SelectedMeshRef->MetalnessValue), 0.0f, 1.0f);
    ImGui::Checkbox("Use Constant Glossiness/Rougness", &(g_SelectedMeshRef->UseGlossinessValue));
    if (g_SelectedMeshRef->UseGlossinessValue) {
        ImGui::SliderFloat("Glossiness", &(g_SelectedMeshRef->GlossinessValue), 0.0f, 1.0f);
        float rougness = 1.0f - g_SelectedMeshRef->GlossinessValue;
        ImGui::SliderFloat("Rougness", &rougness, 0.0f, 1.0f);
        g_SelectedMeshRef->GlossinessValue = 1.0f - rougness;
    }
    ImGui::Separator();

    /* Preset materials section. */
    ImGui::Text("PRESET MATERIALS");
    if (ImGui::Button("Apply Iron")) {
        g_SelectedMeshRef->UseAmbientOcclusionValue = true;
        g_SelectedMeshRef->AmbientOcclusionValue = 1.0f;
        g_SelectedMeshRef->UseAlbedoColor = true;
        g_SelectedMeshRef->AlbedoColor = glm::vec3(0.56f, 0.57f, 0.58f);
        g_SelectedMeshRef->UseMetalnessValue = true;
        g_SelectedMeshRef->MetalnessValue = 1.0f;
        g_SelectedMeshRef->UseGlossinessValue = true;
        g_SelectedMeshRef->GlossinessValue = 0.4f;
    }
    if (ImGui::Button("Apply Silver")) {
        g_SelectedMeshRef->UseAmbientOcclusionValue = true;
        g_SelectedMeshRef->AmbientOcclusionValue = 1.0f;
        g_SelectedMeshRef->UseAlbedoColor = true;
        g_SelectedMeshRef->AlbedoColor = glm::vec3(0.972f, 0.96f, 0.915f);
        g_SelectedMeshRef->UseMetalnessValue = true;
        g_SelectedMeshRef->MetalnessValue = 1.0f;
        g_SelectedMeshRef->UseGlossinessValue = true;
        g_SelectedMeshRef->GlossinessValue = 0.4f;
    }
    if (ImGui::Button("Apply Aluminum")) {
        g_SelectedMeshRef->UseAmbientOcclusionValue = true;
        g_SelectedMeshRef->AmbientOcclusionValue = 1.0f;
        g_SelectedMeshRef->UseAlbedoColor = true;
        g_SelectedMeshRef->AlbedoColor = glm::vec3(0.913f, 0.921f, 0.925f);
        g_SelectedMeshRef->UseMetalnessValue = true;
        g_SelectedMeshRef->MetalnessValue = 1.0f;
        g_SelectedMeshRef->UseGlossinessValue = true;
        g_SelectedMeshRef->GlossinessValue = 0.4f;
    }
    if (ImGui::Button("Apply Gold")) {
        g_SelectedMeshRef->UseAmbientOcclusionValue = true;
        g_SelectedMeshRef->AmbientOcclusionValue = 1.0f;
        g_SelectedMeshRef->UseAlbedoColor = true;
        g_SelectedMeshRef->AlbedoColor = glm::vec3(1.0f, 0.766f, 0.336f);
        g_SelectedMeshRef->UseMetalnessValue = true;
        g_SelectedMeshRef->MetalnessValue = 1.0f;
        g_SelectedMeshRef->UseGlossinessValue = true;
        g_SelectedMeshRef->GlossinessValue = 0.4f;
    }
    if (ImGui::Button("Apply Copper")) {
        g_SelectedMeshRef->UseAmbientOcclusionValue = true;
        g_SelectedMeshRef->AmbientOcclusionValue = 1.0f;
        g_SelectedMeshRef->UseAlbedoColor = true;
        g_SelectedMeshRef->AlbedoColor = glm::vec3(0.955f, 0.637f, 0.538f);
        g_SelectedMeshRef->UseMetalnessValue = true;
        g_SelectedMeshRef->MetalnessValue = 1.0f;
        g_SelectedMeshRef->UseGlossinessValue = true;
        g_SelectedMeshRef->GlossinessValue = 0.4f;
    }
    if (ImGui::Button("Apply Platinum")) {
        g_SelectedMeshRef->UseAmbientOcclusionValue = true;
        g_SelectedMeshRef->AmbientOcclusionValue = 1.0f;
        g_SelectedMeshRef->UseAlbedoColor = true;
        g_SelectedMeshRef->AlbedoColor = glm::vec3(0.672f, 0.637f, 0.585f);
        g_SelectedMeshRef->UseMetalnessValue = true;
        g_SelectedMeshRef->MetalnessValue = 1.0f;
        g_SelectedMeshRef->UseGlossinessValue = true;
        g_SelectedMeshRef->GlossinessValue = 0.4f;
    }
    ImGui::Separator();

    /* Camera controls. */
    ImGui::Text("CAMERA");
    if (ImGui::Button("Go To Object")) {
        g_Camera->Position = glm::vec3(0.0f, 0.0f, 5.0f);
        g_Camera->Rotation = glm::identity<glm::quat>();
    }

    /* End model window. */
    ImGui::End();

    /* Begin scene window. */
    ImGui::SetNextWindowPos(ImVec2((float)w - 300.0f, 0.0f));
    ImGui::SetNextWindowSize(ImVec2(300.0f, (float)h));
    ImGui::SetNextWindowCollapsed(true, ImGuiCond_FirstUseEver);
    ImGui::Begin("Scene", nullptr, ImGuiWindowFlags_NoMove | ImGuiWindowFlags_NoResize | ImGuiWindowFlags_NoNav);

    /* Select environment map. */
    ImGui::Text("ENVIRONMENT MAP");
    if (ImGui::RadioButton("Spruit Sunrise", g_SelectedEnvMap == 0)) {
        g_SelectedEnvMapRef = g_Spruit_Sunrise;
        g_SelectedEnvMap = 0;
    }
    if (ImGui::RadioButton("Cape Hill", g_SelectedEnvMap == 1)) {
        g_SelectedEnvMapRef = g_Cape_Hill;
        g_SelectedEnvMap = 1;
    }
    if (ImGui::RadioButton("Artist Workshop", g_SelectedEnvMap == 2)) {
        g_SelectedEnvMapRef = g_Artist_Workshop;
        g_SelectedEnvMap = 2;
    }
    if ((g_CustomEM.get() != nullptr) && (ImGui::RadioButton("Custom", g_SelectedEnvMap == 3))) {
        g_SelectedEnvMapRef = g_CustomEM;
        g_SelectedEnvMap = 3;
    }
    ImGui::Separator();

    /* Light info. */
    ImGui::Text("LIGHT");
    ImGui::Checkbox("Is Directional", &g_Directional);
    g_PBR->SetLightDirectional(g_Directional);
    ImGui::InputFloat3("Position", glm::value_ptr(g_LightPosition), 2);
    g_PBR->SetLightPosition(g_LightPosition);
    ImGui::InputFloat("Intensity", &g_LightIntensity, 1.0f, 5.0f, 2);
    g_PBR->SetLightIntensity(g_LightIntensity);
    ImGui::ColorPicker3("Color", glm::value_ptr(g_LightColor));
    g_PBR->SetLightColor(g_LightColor);
    ImGui::Separator();

    /* Rendering parameters. */
    ImGui::Text("RENDERING");
    ImGui::SliderFloat("Exposure", &g_Exposure, 0.1f, 5.0f);
    ImGui::SliderFloat("Gamma", &g_Gamma, 0.1f, 5.0f);
    ImGui::Separator();

    /* Display equation debugging. */
    static int equation = 0;
    ImGui::Text("DISPLAY EQUATION (DEBUG)");
    if (ImGui::RadioButton("Full PBR (default)", equation == 0)) {
        g_PBR->SetEquation(equation = 0);
    }
    if (ImGui::RadioButton("Normal Distribution", equation == 1)) {
        g_PBR->SetEquation(equation = 1);
    }
    if (ImGui::RadioButton("Geometry", equation == 2)) {
        g_PBR->SetEquation(equation = 2);
    }
    if (ImGui::RadioButton("Fresnel", equation == 3)) {
        g_PBR->SetEquation(equation = 3);
    }
    if (ImGui::RadioButton("Fresnel (roughness)", equation == 4)) {
        g_PBR->SetEquation(equation = 4);
    }
    if (ImGui::RadioButton("Diffuse IBL", equation == 5)) {
        g_PBR->SetEquation(equation = 5);
    }
    if (ImGui::RadioButton("Specular IBL", equation == 6)) {
        g_PBR->SetEquation(equation = 6);
    }
    ImGui::Separator();

    /* Debugging options. */
    ImGui::Text("DEBUGGING");
    bool disableMSAA = g_DisableMSAA;
    ImGui::Checkbox("Disable MSAA (4x)", &disableMSAA);
    if (g_DisableMSAA != disableMSAA) {
        if (g_DisableMSAA = disableMSAA) {
            glDisable(GL_MULTISAMPLE);
        } else {
            glEnable(GL_MULTISAMPLE);
        }
    }
    bool wireframeOnly = g_WireframeOnly;
    ImGui::Checkbox("Render Wireframe Only", &wireframeOnly);
    if (g_WireframeOnly != wireframeOnly) {
        if (g_WireframeOnly = wireframeOnly) {
            glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
        } else {
            glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
        }
    }
    ImGui::Checkbox("No Environment Map", &g_NoEnvMap);
    bool disableCulling = g_DisableCulling;
    ImGui::Checkbox("Disable Face Culling", &disableCulling);
    if (g_DisableCulling != disableCulling) {
        if (g_DisableCulling = disableCulling) {
            glDisable(GL_CULL_FACE);
        } else {
            glEnable(GL_CULL_FACE);
        }
    }
    ImGui::Separator();

    /* Status print. */
    static float elapsedTime = 0.25f;
    static float fpsMeasured;
    if (elapsedTime >= 0.25f) {
        elapsedTime -= 0.25f;
        fpsMeasured = 1.0f / g_DeltaTime;
    }
    elapsedTime += g_DeltaTime;
    ImGui::Text("STATUS");
    ImGui::Text("FPS: %.0f", fpsMeasured);

    /* End scene window. */
    ImGui::End();

}

int main(int argc, char** argv) {

    /* Initialize GLFW. */
    MUST_SUCCEED(glfwInit(), GLFW_TRUE, "Failed to initialize GLFW.")

    /* Set up context and shader version. */
    const char* glsl_version = "#version 410";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 1);
#ifdef __APPLE__
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
#endif

    /* Create framebuffer with MSAA (4 samples). */
    glfwWindowHint(GLFW_SAMPLES, 4);

    /* Get screen size. */
    GLFWmonitor* monitor;
    MUST_NOT_FAIL(monitor = glfwGetPrimaryMonitor(), nullptr, "Failed to get the primary monitor.")
    const GLFWvidmode* mode;
    MUST_NOT_FAIL(mode = glfwGetVideoMode(monitor), nullptr, "Failed to find screen size.")

    /* Create GLFW window. */
    MUST_NOT_FAIL(g_Window = glfwCreateWindow(800, 600, "PBR Explore", nullptr, nullptr), nullptr, "Failed to create a GLFW window.")
    glfwSetWindowPos(g_Window, (mode->width - 800) / 2, (mode->height - 600) / 2);
    glfwSetWindowSizeLimits(g_Window, 640, 480, GLFW_DONT_CARE, GLFW_DONT_CARE);
    glfwSetWindowSizeCallback(g_Window, ResizeCallback);
    glfwSetScrollCallback(g_Window, MouseWheelCallback);
    glfwSetDropCallback(g_Window, dropFile);
    GLFWimage img;
    img.width = img.height = 128;
    img.pixels = PBRE::g_IconArray;
    glfwSetWindowIcon(g_Window, 1, &img);

    /* Make OpenGL context current. */
    glfwMakeContextCurrent(g_Window);

    /* Initialize GLEW. */
    glewExperimental = true;
    MUST_SUCCEED(glewInit(), GLEW_OK, "Failed to initialize GLEW.")

    /* Initialize Dear ImGui. */
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();
    ImGui::StyleColorsDark();
    ImGui_ImplGlfw_InitForOpenGL(g_Window, true);
    ImGui_ImplOpenGL3_Init(glsl_version);

    /* Disable Dear ImGui configuration file. */
    ImGui::GetIO().IniFilename = nullptr;

    /* Enable depth testing. */
    glEnable(GL_DEPTH_TEST);
    glDepthFunc(GL_LEQUAL);

    /* Enable counter-clockwise culling. */
    glEnable(GL_CULL_FACE);
    glCullFace(GL_CCW);

    /* Enable MSAA. */
    glEnable(GL_MULTISAMPLE);

    /* Load meshes. */
    g_Sphere = std::shared_ptr<PBRE::Mesh>(new PBRE::Mesh(g_ModelPath + "/sphere.fbx"));
    g_Teapot = std::shared_ptr<PBRE::Mesh>(new PBRE::Mesh(g_ModelPath + "/teapot.fbx"));
    g_Monkey = std::shared_ptr<PBRE::Mesh>(new PBRE::Mesh(g_ModelPath + "/monkey.fbx"));

    /* Load environment maps. */
    g_Spruit_Sunrise = std::shared_ptr<PBRE::Texture>(new PBRE::Texture(g_EnvMapPath + "/spruit_sunrise_4k.hdr", PBRE::TextureWrapMode::ClampToEdge, PBRE::TextureWrapMode::ClampToEdge));
    g_Cape_Hill = std::shared_ptr<PBRE::Texture>(new PBRE::Texture(g_EnvMapPath + "/cape_hill_4k.hdr", PBRE::TextureWrapMode::ClampToEdge, PBRE::TextureWrapMode::ClampToEdge));
    g_Artist_Workshop = std::shared_ptr<PBRE::Texture>(new PBRE::Texture(g_EnvMapPath + "/artist_workshop_4k.hdr", PBRE::TextureWrapMode::ClampToEdge, PBRE::TextureWrapMode::ClampToEdge));
    g_SelectedEnvMapRef = g_Spruit_Sunrise;

    /* Initialize scene. */
    PBRE::Scene scene;
    g_Scene = &scene;
    PBRE::Spatial* root = scene.GetRoot();

    /* Initialize camera. */
    g_Camera = root->Add(glm::vec3(-1.0f, 0.0f, 5.0f));

    /* Initialize mesh. */
    g_Child = root->Add();
    g_Child->Rotate(glm::vec3(0.0f, 1.0f, 0.0f), glm::pi<float>());
    g_SelectedMeshRef = g_Sphere;
    g_Child->SetMesh(g_Sphere);

    /* Initialize PBR program. */
    PBRE::Program pbrProg("pbr");
    g_PBR = &pbrProg;
    g_PBR->SetEquation(0);

    /* Update projection and view matrix. */
    UpdateProjection(800, 600);
    UpdateView();

    /* Initialize environment map program. */
    PBRE::Program envMapProg("envmap");

    /* Initialize environment map mesh. */
    glm::vec3 verts[] = {
        glm::vec3(1.0f, 1.0f, -1.0f),
        glm::vec3(-1.0f, 1.0f, -1.0f),
        glm::vec3(-1.0f, -1.0f, -1.0f),
        glm::vec3(1.0f, -1.0f, -1.0f)
    };
    glm::vec2 uvs[4];
    glm::vec3 normals[4];
    glm::uvec3 indices[] = {
        glm::uvec3(0, 1, 2),
        glm::uvec3(0, 2, 3),
    };
    PBRE::Mesh envMapMesh(verts, uvs, normals, 4, indices, 2);

    /* Load BRDF look-up table. */
    g_BRDFResponse = std::shared_ptr<PBRE::Texture>(new PBRE::Texture("brdf/splitsum.hdr", PBRE::TextureWrapMode::ClampToEdge, PBRE::TextureWrapMode::ClampToEdge, PBRE::TextureFilter::Bilinear, PBRE::TextureFilter::Nearest));
    g_PBR->SetBrdfResponseTexture(*(g_BRDFResponse.get()));

    /* Main loop. */
    float previousFrame = glfwGetTime();
    while (!glfwWindowShouldClose(g_Window)) {
        
        /* Poll window events. */
        glfwPollEvents();

        /* Calculate delta time. */
        float currentTime = glfwGetTime();
        g_DeltaTime = currentTime - previousFrame;
        previousFrame = currentTime;

        /* Handle camera. */
        HandleCameraMovement();

        /* Rotate child. */
        if (g_AutoRotate) {
            g_Child->Rotate(glm::vec3(0.0f, 1.0f, 0.0f), g_DeltaTime * g_RotationSpeed);
        }

        /* Render scene. */
        pbrProg.RenderScene(scene, g_SelectedEnvMapRef, g_Exposure, g_Gamma, g_NoEnvMap);

        /* Render environment map. */
        if (!g_WireframeOnly && !g_NoEnvMap) {
            int w, h;
            glfwGetWindowSize(g_Window, &w, &h);
            envMapProg.RenderMesh(envMapMesh, g_Camera->GetRotationMatrix(), glm::scale(glm::mat4(1.0f), glm::vec3((float)w / h, 1.0f, 1.0f)), g_SelectedEnvMapRef, g_Exposure, g_Gamma);
        }

        /* Start GUI frame. */
        ImGui_ImplOpenGL3_NewFrame();
        ImGui_ImplGlfw_NewFrame();
        ImGui::NewFrame();

        /* Draw GUI elements. */
        DrawGUI();

        /* Render GUI. */
        ImGui::Render();
        ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());

        /* Swap buffers. */
        glfwSwapBuffers(g_Window);

    }

    /* Terminate GLFW. */
    glfwTerminate();

    return 0;

}
