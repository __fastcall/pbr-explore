#include <PBRE/Texture.hxx>
#include <PBRE/MacroHelpers.hxx>
#include <STB/stb_image.h>

namespace PBRE {

    Texture::Texture(const std::string& imagepath, TextureWrapMode s, TextureWrapMode t, TextureFilter min, TextureFilter mag) {

        /* Initialize variables. */
        int w, h, n;
        unsigned char* dataLdr = nullptr;
        float* dataHdr = nullptr;
        bool isHdr = stbi_is_hdr(imagepath.c_str());

        /* Flip on vertical axis. */
        stbi_set_flip_vertically_on_load(true);

        /* Load HDR or LDR. */
        if (isHdr) {
            SHOULD_NOT_FAIL(dataHdr = stbi_loadf(imagepath.c_str(), &w, &h, &n, 3), nullptr, "Failed to load the image file.")
        } else {
            SHOULD_NOT_FAIL(dataLdr = stbi_load(imagepath.c_str(), &w, &h, &n, 3), nullptr, "Failed to load the image file.")
        }

        /* Creates a texture. */
        glGenTextures(1, &m_Texture);
        glBindTexture(GL_TEXTURE_2D, m_Texture);

        /* Set texture parameters. */
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GetInternalWrapMode(s));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GetInternalWrapMode(t));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GetInternalFilter(min));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GetInternalFilter(mag));

        /* Load texture data and free buffers. */
        if (isHdr) {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB16, w, h, 0, GL_RGB, GL_FLOAT, dataHdr);
            stbi_image_free(dataHdr);
        } else {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, dataLdr);
            stbi_image_free(dataLdr);
        }

        /* Generate mipmaps. */
        glGenerateMipmap(GL_TEXTURE_2D);

        /* Unbind texture. */
        glBindTexture(GL_TEXTURE_2D, 0);

    }

    Texture::Texture(Texture&& texture) {

        /* Move native texture handle. */
        m_Texture = texture.m_Texture;
        texture.m_Texture = 0;

    }

    Texture& Texture::operator=(Texture&& texture) {

        /* Move native texture handle. */
        glDeleteTextures(1, &m_Texture);
        m_Texture = texture.m_Texture;
        texture.m_Texture = 0;
        return *this;

    }

    Texture::~Texture() {

        /* Destroys native texture handle. */
        glDeleteTextures(1, &m_Texture);
        m_Texture = 0;

    }

    void Texture::SetTextureWrapMode(TextureWrapMode s, TextureWrapMode t) {

        /* Set texture wrap mode. */
        glBindTexture(GL_TEXTURE_2D, m_Texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GetInternalWrapMode(s));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GetInternalWrapMode(t));
        glBindTexture(GL_TEXTURE_2D, 0);

    }

    void Texture::SetTextureFilter(TextureFilter min, TextureFilter max) {

        /* Set texture filter. */
        glBindTexture(GL_TEXTURE_2D, m_Texture);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GetInternalFilter(min));
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GetInternalFilter(max));
        glBindTexture(GL_TEXTURE_2D, 0);

    }

    GLenum Texture::GetInternalWrapMode(TextureWrapMode mode) {

        /* Decode wrap mode constant. */
        switch (mode) {
        case TextureWrapMode::Repeat:
            return GL_REPEAT;
        case TextureWrapMode::MirroredRepeat:
            return GL_MIRRORED_REPEAT;
        case TextureWrapMode::ClampToEdge:
            return GL_CLAMP_TO_EDGE;
        }
        return 0;

    }

    GLenum Texture::GetInternalFilter(TextureFilter filter) {

        /* Decode filter constant. */
        switch (filter) {
        case TextureFilter::Nearest:
            return GL_NEAREST;
        case TextureFilter::Linear:
        case TextureFilter::Bilinear:
            return GL_LINEAR;
        case TextureFilter::NearestMipmapNearest:
            return GL_NEAREST_MIPMAP_NEAREST;
        case TextureFilter::NearestMipmapLinear:
            return GL_NEAREST_MIPMAP_LINEAR;
        case TextureFilter::LinearMipmapNearest:
        case TextureFilter::BilinearWithMipmaps:
            return GL_LINEAR_MIPMAP_NEAREST;
        case TextureFilter::LinearMipmapLinear:
        case TextureFilter::Trilinear:
            return GL_LINEAR_MIPMAP_LINEAR;
        }
        return 0;

    }

}
