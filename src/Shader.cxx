#include <PBRE/Shader.hxx>

namespace PBRE {

    Shader::Shader(const std::string& sourcefile, ShaderType type) {

        /* Create shader handle. */
        m_ShaderHandle = std::shared_ptr<ShaderHandle>(new ShaderHandle(sourcefile, type));

    }

    Shader::Shader(const Shader& shader) {

        /* Copy shader. */
        m_ShaderHandle = shader.m_ShaderHandle;

    }

    Shader::Shader(Shader&& shader) {

        /* Move shader. */
        m_ShaderHandle = std::move(shader.m_ShaderHandle);
        
    }

    Shader& Shader::operator=(const Shader& shader) {

        /* Copy shader. */
        m_ShaderHandle = shader.m_ShaderHandle;
        return *this;

    }

    Shader& Shader::operator=(Shader&& shader) {

        /* Move shader. */
        m_ShaderHandle = std::move(shader.m_ShaderHandle);
        return *this;

    }

}
