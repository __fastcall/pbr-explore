#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <assimp/material.h>
#include <assimp/mesh.h>
#include <PBRE/MacroHelpers.hxx>
#include <PBRE/Mesh.hxx>
#include <vector>

namespace PBRE {

    Mesh::Mesh(const std::string& filename) {

        /* Load model. */
        Assimp::Importer importer;
        const aiScene* scene;
        SHOULD_NOT_FAIL(scene = importer.ReadFile(filename, aiProcess_Triangulate), nullptr, "Failed to load model file.");

        /* Assure there is at least one mesh. */
        SHOULD_NOT_FAIL(scene->mNumMeshes, 0, "Model does not contain any meshes.")

        /* Find model path. */
#ifdef IS_WINDOWS
        int wPos = filename.rfind("\\");
#else
        int wPos = -1;
#endif
        int xPos = filename.rfind("/");
        int pos = (wPos > xPos) ? wPos : xPos;
        std::string prepath = "";
        if (pos >= 0) prepath = filename.substr(0, pos + 1);

        /* Get material. */
        const aiMesh* mesh = scene->mMeshes[0];
        const aiMaterial* material = scene->mMaterials[mesh->mMaterialIndex];

        /* Load ambient occlusion texture if available. */
        AmbientOcclusionValue = 1.0f;
        if (material->GetTextureCount(aiTextureType_LIGHTMAP) > 0) {
            aiString filename;
            aiTextureMapMode mmode;
            material->GetTexture(aiTextureType_LIGHTMAP, 0, &filename, nullptr, nullptr, nullptr, nullptr, &mmode);
            TextureWrapMode wrap = TextureWrapMode::Repeat;
            switch (mmode) {
            case aiTextureMapMode_Clamp:
                wrap = TextureWrapMode::ClampToEdge;
                break;
            case aiTextureMapMode_Wrap:
                wrap = TextureWrapMode::Repeat;
                break;
            case aiTextureMapMode_Mirror:
                wrap = TextureWrapMode::MirroredRepeat;
                break;
            }
            m_AmbientOcclusionTexture = std::unique_ptr<Texture>(new Texture(prepath + filename.data, wrap, wrap));
            UseAmbientOcclusionValue = false;
        } else {
            UseAmbientOcclusionValue = true;
        }

        /* Load albedo texture if available. */
        AlbedoColor = glm::vec3(1.0f, 1.0f, 1.0f);
        if (material->GetTextureCount(aiTextureType_DIFFUSE) > 0) {
            aiString filename;
            aiTextureMapMode mmode;
            material->GetTexture(aiTextureType_DIFFUSE, 0, &filename, nullptr, nullptr, nullptr, nullptr, &mmode);
            TextureWrapMode wrap = TextureWrapMode::Repeat;
            switch (mmode) {
            case aiTextureMapMode_Clamp:
                wrap = TextureWrapMode::ClampToEdge;
                break;
            case aiTextureMapMode_Wrap:
                wrap = TextureWrapMode::Repeat;
                break;
            case aiTextureMapMode_Mirror:
                wrap = TextureWrapMode::MirroredRepeat;
                break;
            }
            m_AlbedoTexture = std::unique_ptr<Texture>(new Texture(prepath + filename.data, wrap, wrap));
            UseAlbedoColor = false;
        } else {
            UseAlbedoColor = true;
        }

        /* Load metalness texture if available. */
        MetalnessValue = 0.0f;
        if (material->GetTextureCount(aiTextureType_SPECULAR) > 0) {
            aiString filename;
            aiTextureMapMode mmode;
            material->GetTexture(aiTextureType_SPECULAR, 0, &filename, nullptr, nullptr, nullptr, nullptr, &mmode);
            TextureWrapMode wrap = TextureWrapMode::Repeat;
            switch (mmode) {
            case aiTextureMapMode_Clamp:
                wrap = TextureWrapMode::ClampToEdge;
                break;
            case aiTextureMapMode_Wrap:
                wrap = TextureWrapMode::Repeat;
                break;
            case aiTextureMapMode_Mirror:
                wrap = TextureWrapMode::MirroredRepeat;
                break;
            }
            m_MetalnessTexture = std::unique_ptr<Texture>(new Texture(prepath + filename.data, wrap, wrap));
            UseMetalnessValue = false;
        } else {
            UseMetalnessValue = true;
        }

        /* Load glossiness (1 - rougness) texture if available. */
        GlossinessValue = 0.0f;
        if (material->GetTextureCount(aiTextureType_SHININESS) > 0) {
            aiString filename;
            aiTextureMapMode mmode;
            material->GetTexture(aiTextureType_SHININESS, 0, &filename, nullptr, nullptr, nullptr, nullptr, &mmode);
            TextureWrapMode wrap = TextureWrapMode::Repeat;
            switch (mmode) {
            case aiTextureMapMode_Clamp:
                wrap = TextureWrapMode::ClampToEdge;
                break;
            case aiTextureMapMode_Wrap:
                wrap = TextureWrapMode::Repeat;
                break;
            case aiTextureMapMode_Mirror:
                wrap = TextureWrapMode::MirroredRepeat;
                break;
            }
            m_GlossinessTexture = std::unique_ptr<Texture>(new Texture(prepath + filename.data, wrap, wrap));
            UseGlossinessValue = false;
        } else {
            UseGlossinessValue = true;
        }

        /* Reserve data for vertices. */
        std::vector<float> vertData;
        vertData.reserve(8 * mesh->mNumVertices);

        /* Reserve data for indices. */
        std::vector<unsigned int> indData;
        m_Count = 3 * mesh->mNumFaces;
        indData.reserve(m_Count);

        /* Is UV mapping provided? */
        bool hasUvChannel = (mesh->GetNumUVChannels() > 0) && (mesh->HasTextureCoords(0));

        /* Load vertices data. */
        for (int i = 0; i < mesh->mNumVertices; ++i) {
            vertData.push_back(mesh->mVertices[i].x);
            vertData.push_back(mesh->mVertices[i].y);
            vertData.push_back(mesh->mVertices[i].z);
            if (hasUvChannel) {
                vertData.push_back(mesh->mTextureCoords[0][i].x);
                vertData.push_back(mesh->mTextureCoords[0][i].y);
            } else {
                vertData.push_back(0.0f);
                vertData.push_back(0.0f);
            }
            vertData.push_back(mesh->mNormals[i].x);
            vertData.push_back(mesh->mNormals[i].y);
            vertData.push_back(mesh->mNormals[i].z);
        }

        /* Load indices data. */
        for (int i = 0; i < mesh->mNumFaces; ++i) {
            indData.push_back(mesh->mFaces[i].mIndices[0]);
            indData.push_back(mesh->mFaces[i].mIndices[1]);
            indData.push_back(mesh->mFaces[i].mIndices[2]);
        }

        /* Creates and binds vertex array object. */
        glCreateVertexArrays(1, &m_VAO);
        glBindVertexArray(m_VAO);

        /* Generate buffer objects. */
        glGenBuffers(1, &m_VBO);
        glGenBuffers(1, &m_EBO);

        /* Bind buffers. */
        glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);

        /* Load buffer data. */
        glBufferData(GL_ARRAY_BUFFER, vertData.size() * sizeof(float), vertData.data(), GL_STATIC_DRAW);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indData.size() * sizeof(unsigned int), indData.data(), GL_STATIC_DRAW);

        /* Set up attribute mapping. */
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), reinterpret_cast<void*>(0));
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), reinterpret_cast<void*>(3 * sizeof(float)));
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), reinterpret_cast<void*>(5 * sizeof(float)));
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);

        /* Unbind vertex array object. */
        glBindVertexArray(0);

    }

    Mesh::Mesh(const glm::vec3* verts, const glm::vec2* uvs, const glm::vec3* normals, std::size_t nVerts, const glm::uvec3* indices, std::size_t nIndices) {

        /* Initialize parameters. */
        AmbientOcclusionValue = 1.0f;
        UseAmbientOcclusionValue = true;
        AlbedoColor = glm::vec3(1.0f, 1.0f, 1.0f);
        UseAlbedoColor = true;
        MetalnessValue = 0.0f;
        UseMetalnessValue = true;
        GlossinessValue = 0.0f;
        UseGlossinessValue = true;

        /* Reserve data for vertices. */
        std::vector<float> vertData;
        vertData.reserve(8 * nVerts);

        /* Reserve data for indices. */
        std::vector<unsigned int> indData;
        m_Count = 3 * nIndices;
        indData.reserve(m_Count);

        /* Construct vertex buffer object array. */
        for (std::size_t i = 0; i < nVerts; ++i) {
            vertData.push_back(verts[i].x);
            vertData.push_back(verts[i].y);
            vertData.push_back(verts[i].z);
            vertData.push_back(uvs[i].x);
            vertData.push_back(uvs[i].y);
            vertData.push_back(normals[i].x);
            vertData.push_back(normals[i].y);
            vertData.push_back(normals[i].z);
        }

        /* Construct element buffer object array. */
        for (std::size_t i = 0; i < nIndices; ++i) {
            indData.push_back(indices[i].x);
            indData.push_back(indices[i].y);
            indData.push_back(indices[i].z);
        }

        /* Creates and binds vertex array object. */
        glCreateVertexArrays(1, &m_VAO);
        glBindVertexArray(m_VAO);

        /* Generate buffer objects. */
        glGenBuffers(1, &m_VBO);
        glGenBuffers(1, &m_EBO);

        /* Bind buffers. */
        glBindBuffer(GL_ARRAY_BUFFER, m_VBO);
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, m_EBO);

        /* Load buffer data. */
        glBufferData(GL_ARRAY_BUFFER, vertData.size() * sizeof(float), vertData.data(), GL_STATIC_DRAW);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, indData.size() * sizeof(unsigned int), indData.data(), GL_STATIC_DRAW);

        /* Set up attribute mapping. */
        glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), reinterpret_cast<void*>(0));
        glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), reinterpret_cast<void*>(3 * sizeof(float)));
        glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), reinterpret_cast<void*>(5 * sizeof(float)));
        glEnableVertexAttribArray(0);
        glEnableVertexAttribArray(1);
        glEnableVertexAttribArray(2);

        /* Unbind vertex array object. */
        glBindVertexArray(0);

    }

    Mesh::Mesh(Mesh&& mesh) {

        /* Move values. */
        AmbientOcclusionValue = mesh.AmbientOcclusionValue;
        UseAmbientOcclusionValue = mesh.UseAmbientOcclusionValue;
        AlbedoColor = mesh.AlbedoColor;
        UseAlbedoColor = mesh.UseAlbedoColor;
        MetalnessValue = mesh.MetalnessValue;
        UseMetalnessValue = mesh.UseMetalnessValue;
        GlossinessValue = mesh.GlossinessValue;
        UseGlossinessValue = mesh.UseGlossinessValue;

        /* Move textures. */
        m_AmbientOcclusionTexture = std::move(mesh.m_AmbientOcclusionTexture);
        m_AlbedoTexture = std::move(mesh.m_AlbedoTexture);
        m_MetalnessTexture = std::move(mesh.m_MetalnessTexture);
        m_GlossinessTexture = std::move(mesh.m_GlossinessTexture);

        /* Moves native handles. */
        m_VAO = mesh.m_VAO;
        mesh.m_VAO = 0;
        m_VBO = mesh.m_VBO;
        mesh.m_VBO = 0;
        m_EBO = mesh.m_EBO;
        mesh.m_EBO = 0;

        /* Move element count. */
        m_Count = mesh.m_Count;

    }

    Mesh& Mesh::operator=(Mesh&& mesh) {

        /* Move values. */
        AmbientOcclusionValue = mesh.AmbientOcclusionValue;
        UseAmbientOcclusionValue = mesh.UseAmbientOcclusionValue;
        AlbedoColor = mesh.AlbedoColor;
        UseAlbedoColor = mesh.UseAlbedoColor;
        MetalnessValue = mesh.MetalnessValue;
        UseMetalnessValue = mesh.UseMetalnessValue;
        GlossinessValue = mesh.GlossinessValue;
        UseGlossinessValue = mesh.UseGlossinessValue;

        /* Move textures. */
        m_AmbientOcclusionTexture = std::move(mesh.m_AmbientOcclusionTexture);
        m_AlbedoTexture = std::move(mesh.m_AlbedoTexture);
        m_MetalnessTexture = std::move(mesh.m_MetalnessTexture);
        m_GlossinessTexture = std::move(mesh.m_GlossinessTexture);

        /* Moves native handles. */
        glDeleteVertexArrays(1, &m_VAO);
        m_VAO = mesh.m_VAO;
        mesh.m_VAO = 0;
        glDeleteBuffers(1, &m_VBO);
        m_VBO = mesh.m_VBO;
        mesh.m_VBO = 0;
        glDeleteBuffers(1, &m_EBO);
        m_EBO = mesh.m_EBO;
        mesh.m_EBO = 0;

        /* Move element count. */
        m_Count = mesh.m_Count;

        /* Return reference to this. */
        return *this;
        
    }

    Mesh::~Mesh() {

        /* Destroys native handles. */
        glDeleteBuffers(1, &m_EBO);
        glDeleteBuffers(1, &m_VBO);
        glDeleteVertexArrays(1, &m_VAO);
        m_EBO = 0;
        m_VBO = 0;
        m_VAO = 0;

    }

}
