# PBR Explore

![PBR Explore](window_demo.png "PBR Explore")

## About

This tool can be used to preview how materials behave in a physically-based rendering workflow.

## Usage

Simply open the program and adjust parameters to your liking to see how they behave in a PBR workflow.

## Building

### CMake build system

Building is handled using CMake build system. Make sure you have [CMake](https://cmake.org/) installed on your machine.

### List of dependencies

- OpenGL
- [GLEW](http://glew.sourceforge.net/)
- [GLFW](https://www.glfw.org/)
- [GLM](https://glm.g-truc.net/)
- [assimp](http://www.assimp.org/)
- [Dear ImGui](https://github.com/ocornut/imgui)

### Configuration variables

| **Variable name**     | **Description**                                                                                                                 | **Default value**                    |
|-----------------------|---------------------------------------------------------------------------------------------------------------------------------|--------------------------------------|
| `OPENGL_INCLUDE_DIRS` | Path to OpenGL include directory.                                                                                               | *Should be discovered automatically* |
| `OPENGL_LIBRARIES`    | Path to OpenGL library.                                                                                                         | *Should be discovered automatically* |
| `GLEW_INCLUDE_DIRS`   | Path to GLEW include directory.                                                                                                 | *Should be discovered automatically* |
| `GLEW_LIBRARIES`      | Path to GLEW library.                                                                                                           | *Should be discovered automatically* |
| `GLEW_LINK_STATIC`    | Is the GLEW library compiled as static library. Defines `GLEW_STATIC` at compile time in all source files if set to `ON`.       | `ON` *on Windows,* `OFF` *otherwise* |
| `GLFW_INCLUDE_DIRS`   | Path to GLFW include directory.                                                                                                 | *Should be discovered automatically* |
| `GLFW_LIBRARIES`      | Path to GLFW library.                                                                                                           | *Should be discovered automatically* |
| `GLM_INCLUDE_DIRS`    | Path to GLM include directory.                                                                                                  | *Should be discovered automatically* |
| `assimp_INCLUDE_DIRS` | Path to assimp include directory.                                                                                               | *Should be discovered automatically* |
| `assimp_LIBRARIES`    | Path to assimp library.                                                                                                         | *Should be discovered automatically* |
| `IMGUI_SRC_DIR`       | Path to Dear ImGui source directory. Uses files `imgui.cpp`, `imgui_draw.cpp` and `imgui_widgets.cpp` as well as their headers. | *Empty string*                       |
| `IMGUI_EXAMPLES_DIR`  | Path to Dear ImGui examples directory. Uses files `imgui_impl_opengl3.cpp` and `imgui_impl_glfw.cpp` as well as their headers.  | *Empty string*                       |
| `GENERATE_DOC`        | Generates documentation (requires Doxygen).                                                                                     | `OFF`                                |

### Generating build files

First clone this repository on your machine, then position inside the downloaded repository.

```
git clone https://gitlab.com/__fastcall/pbr-explore pbre
cd pbre
```

Now create a directory where you plan to generate the build files of your project and run the CMake command. In the example below I will generate Visual Studio solution and I'll set only the `IMGUI_SRC_DIR` and `IMGUI_EXAMPLES_DIR` variables since the other variables could be detected automatically.

```
mkdir build && cd build
cmake -G"Visual Studio 15 2017 Win64" -DIMGUI_SRC_DIR="../../imgui" -DIMGUI_EXAMPLES_DIR="../../imgui/examples" ..
```

Note that you should also set the `CMAKE_BUILD_TYPE` to Release to omit debugging informations from target and produce smaller executable. If your build tool is multi-configuration, like Visual Studio, you can choose the configuration at build time like this.

```
cmake --build . --config Release
```

## Prebuilt release

You can download the latest release from [here](https://gitlab.com/__fastcall/pbr-explore/tags).

## Author

Created by Antonio J. as a project for Faculty of Electrical Engineering and Computing (FER).
